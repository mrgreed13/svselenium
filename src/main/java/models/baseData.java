package models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class baseData {
    private int pageSize;
    private int pageNumber;
    private Double lat;
    private Double lon;
    }
