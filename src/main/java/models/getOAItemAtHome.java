package models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class getOAItemAtHome {
        private String requestType;
        private String language;
        private String userIdApp;
        private baseData data;
}
