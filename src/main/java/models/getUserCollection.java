package models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class getUserCollection {
    private String requestType;
    private String language;
    private String userIdApp;
    private collectionData data;
}
