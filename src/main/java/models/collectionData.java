package models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class collectionData {
    private int pageSize;
    private int pageNumber;
    private String sort;
}
