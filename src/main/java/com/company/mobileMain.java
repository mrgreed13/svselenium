package com.company;

import com.company.Pages.*;
import org.testng.Assert;
import org.testng.annotations.Test;

public class mobileMain extends mobileBasePage {
    @Test
    public static void verify_searchTDMMMiniApp() {
        mobileOnboardingPage obPage = new mobileOnboardingPage();
        obPage.tapOnKPBtn();

        mobileLoginUserPhoneNumberPage loginPhoneNumberPage = new mobileLoginUserPhoneNumberPage();
        loginPhoneNumberPage.inputUserMobileNumber(userWallet);

        mobileInputOTPPage otpPage = new mobileInputOTPPage();
        otpPage.inputOTP(loginOTP);

        mobileLoginUserPasswordPage loginPasswordPage = new mobileLoginUserPasswordPage();
        loginPasswordPage.inputUserPassword(userPassword);

        mobileHomePlatformPage homePlatformPage = new mobileHomePlatformPage();
        homePlatformPage.closePopup();
        homePlatformPage.accessSearchPage();

        mobileGlobalSearchPage globalSearchPage = new mobileGlobalSearchPage();
        globalSearchPage.searchMiniApp(tdmmKeyWord);
        Assert.assertEquals(tdmmKeyWord, globalSearchPage.getMiniAppName());
    }

    @Test
    public static void verifyAPIatTDMMHome(){
        mobileAPI apiApp = new mobileAPI();
        apiApp.verifyGetOAAtHomeAPI();
    }

    @Test
    public static void verifyAPIatMyCollection(){
        mobileAPI apiApp = new mobileAPI();
        apiApp.verifyGetUserCollection();
    }



    //outdated
    //should assert OA name with OA name list from API
    @Test
    public static void verify_accessTDMMMiniApp() {
        mobileOnboardingPage obPage = new mobileOnboardingPage();
        obPage.tapOnKPBtn();

        mobileLoginUserPhoneNumberPage loginPhoneNumberPage = new mobileLoginUserPhoneNumberPage();
        loginPhoneNumberPage.inputUserMobileNumber(userWallet);

        mobileInputOTPPage otpPage = new mobileInputOTPPage();
        otpPage.inputOTP(loginOTP);

        mobileLoginUserPasswordPage loginPasswordPage = new mobileLoginUserPasswordPage();
        loginPasswordPage.inputUserPassword(userPassword);

        mobileHomePlatformPage homePlatformPage = new mobileHomePlatformPage();
        homePlatformPage.closePopup();
        homePlatformPage.accessSearchPage();

        mobileGlobalSearchPage globalSearchPage = new mobileGlobalSearchPage();
        globalSearchPage.searchMiniApp(tdmmKeyWord);
        globalSearchPage.tapOnResult();

        mobileTDMMHomePage tdmmHomePage = new mobileTDMMHomePage();
        tdmmHomePage.closeLoactionPopup();
        Assert.assertEquals(defaultLocation, tdmmHomePage.getDefaultLocation());

        mobileAPI apiApp = new mobileAPI();
        Assert.assertEquals(apiApp.getTheFisrtOAName(), tdmmHomePage.getTheFirstOAName());
    }

    @Test
    public static void verify_accessAccountPage() {
        mobileOnboardingPage obPage = new mobileOnboardingPage();
        obPage.tapOnKPBtn();

        mobileLoginUserPhoneNumberPage loginPhoneNumberPage = new mobileLoginUserPhoneNumberPage();
        loginPhoneNumberPage.inputUserMobileNumber(userWallet);

        mobileInputOTPPage otpPage = new mobileInputOTPPage();
        otpPage.inputOTP(loginOTP);

        mobileLoginUserPasswordPage loginPasswordPage = new mobileLoginUserPasswordPage();
        loginPasswordPage.inputUserPassword(userPassword);

        mobileHomePlatformPage homePlatformPage = new mobileHomePlatformPage();
        homePlatformPage.closePopup();
        homePlatformPage.accessSearchPage();

        mobileGlobalSearchPage globalSearchPage = new mobileGlobalSearchPage();
        globalSearchPage.searchMiniApp(tdmmKeyWord);
        globalSearchPage.tapOnResult();

        mobileTDMMHomePage tdmmHomePage = new mobileTDMMHomePage();
        tdmmHomePage.closeLoactionPopup();
        tdmmHomePage.tapOnWishIcon("Tôi");

        mobileAccountPage tdmmAccountPage = new mobileAccountPage();
        Assert.assertEquals(accountTitle, tdmmAccountPage.getAccountTitlePage());
        Assert.assertEquals(bstCuaToiCTA, tdmmAccountPage.getMyCollectionCTA());
        Assert.assertEquals("VAN TRAN", tdmmAccountPage.getUserName());
    }

    @Test
    public static void verify_accessMyCollectionPage() {
        mobileOnboardingPage obPage = new mobileOnboardingPage();
        obPage.tapOnKPBtn();

        mobileLoginUserPhoneNumberPage loginPhoneNumberPage = new mobileLoginUserPhoneNumberPage();
        loginPhoneNumberPage.inputUserMobileNumber(userWallet);

        mobileInputOTPPage otpPage = new mobileInputOTPPage();
        otpPage.inputOTP(loginOTP);

        mobileLoginUserPasswordPage loginPasswordPage = new mobileLoginUserPasswordPage();
        loginPasswordPage.inputUserPassword(userPassword);

        mobileHomePlatformPage homePlatformPage = new mobileHomePlatformPage();
        homePlatformPage.closePopup();
        homePlatformPage.accessSearchPage();

        mobileGlobalSearchPage globalSearchPage = new mobileGlobalSearchPage();
        globalSearchPage.searchMiniApp(tdmmKeyWord);
        globalSearchPage.tapOnResult();

        mobileTDMMHomePage tdmmHomePage = new mobileTDMMHomePage();
        tdmmHomePage.closeLoactionPopup();
        tdmmHomePage.tapOnWishIcon("Tôi");

        mobileAccountPage tdmmAccountPage = new mobileAccountPage();
        tdmmAccountPage.accessMyCollectionPage();

        mobileMyCollectionPage tdmmMyCollectionPage = new mobileMyCollectionPage();
        Assert.assertEquals(bstCuaToiCTA, tdmmMyCollectionPage.getMyCollectionTitle());
        Assert.assertEquals(searchPlaceHolder, tdmmMyCollectionPage.getSearchPlaceHolder());
        Assert.assertEquals(defaultCollection, tdmmMyCollectionPage.getDefaultCollection());
        Assert.assertEquals(tatCaBtn, tdmmMyCollectionPage.getDefaultFilterBtn());
        Assert.assertEquals(cuaToiBtn, tdmmMyCollectionPage.getMyOwnBtn());
        Assert.assertEquals(topTrendingBtn, tdmmMyCollectionPage.getTopTrendingBtn());
    }

    @Test
    public static void verify_accessCreateANewCollectionPage() {
        mobileOnboardingPage obPage = new mobileOnboardingPage();
        obPage.tapOnKPBtn();

        mobileLoginUserPhoneNumberPage loginPhoneNumberPage = new mobileLoginUserPhoneNumberPage();
        loginPhoneNumberPage.inputUserMobileNumber(userWallet);

        mobileInputOTPPage otpPage = new mobileInputOTPPage();
        otpPage.inputOTP(loginOTP);

        mobileLoginUserPasswordPage loginPasswordPage = new mobileLoginUserPasswordPage();
        loginPasswordPage.inputUserPassword(userPassword);

        mobileHomePlatformPage homePlatformPage = new mobileHomePlatformPage();
        homePlatformPage.closePopup();
        homePlatformPage.accessSearchPage();

        mobileGlobalSearchPage globalSearchPage = new mobileGlobalSearchPage();
        globalSearchPage.searchMiniApp(tdmmKeyWord);
        globalSearchPage.tapOnResult();

        mobileTDMMHomePage tdmmHomePage = new mobileTDMMHomePage();
        tdmmHomePage.closeLoactionPopup();
        tdmmHomePage.tapOnWishIcon("Tôi");

        mobileAccountPage tdmmAccountPage = new mobileAccountPage();
        tdmmAccountPage.accessMyCollectionPage();

        mobileMyCollectionPage tdmmMyCollectionPage = new mobileMyCollectionPage();
        tdmmMyCollectionPage.tapOnCreateANewCollectionBtn();

        mobileCreateANewCollectionPage tdmmCreateANewCollectionPage = new mobileCreateANewCollectionPage();
        tdmmCreateANewCollectionPage.createANewCollection(newCollectionName);
        Assert.assertEquals(newCollectionName, tdmmMyCollectionPage.getNewCollectionName());

/*        Assert.assertEquals(taoBSTMoiTitle, tdmmCreateANewCollectionPage.getPageTitle());
        Assert.assertEquals(inputTitle, tdmmCreateANewCollectionPage.getInputTitleText());
        Assert.assertEquals(inputPlaceHolder, tdmmCreateANewCollectionPage.getInputPlaceHolderText());
        Assert.assertEquals(createBtn, tdmmCreateANewCollectionPage.getCreateBtn());*/

        mobileAPI apiApp = new mobileAPI();
        Assert.assertEquals(newCollectionName, apiApp.getTheNewestCollection());

    }
}