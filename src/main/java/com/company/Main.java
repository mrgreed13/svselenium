package com.company;

import com.company.Pages.*;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Main extends BasePage {

    @Test
    public void test_loginSuccess() {
        LandingPage landingPage = new LandingPage();
        landingPage.clickOnLoginAndSignUpButton();

        LoginPage loginPage = new LoginPage();
        loginPage.loginWithValue("0938765013", "");
        String checkResult = loginPage.checkLoginResult();
        System.out.println(checkResult);
    }

/*    @Test
    public void test_updateProfilePage_fullName() {
        LandingPage landingPage = new LandingPage();
        landingPage.clickOnLoginAndSignUpButton();

        LoginPage loginPage = new LoginPage();
        loginPage.loginWithValue("0938765013", "123456a@A");

        LandingPageAfterLoginSuccess landingPageAfterLoginSuccess = new LandingPageAfterLoginSuccess();
        landingPageAfterLoginSuccess.clickOnAccountSection();

        //Please input a new value for full name field
        String newFullName = "qưertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfg";
        ProfilePage profilePage = new ProfilePage();
        profilePage.inputNewFullName(newFullName);
        profilePage.verifyFullName(newFullName);

    }*/

/*    @Test
    public void test_userCanUpdateDOB() {
        LandingPage landingPage = new LandingPage();
        landingPage.clickOnLoginAndSignUpButton();

        LoginPage loginPage = new LoginPage();
        loginPage.login("0938765013", "123456a@A");

        LandingPageAfterLoginSuccess landingPageAfterLoginSuccess = new LandingPageAfterLoginSuccess();
        landingPageAfterLoginSuccess.clickOnAccountSection();

        String newDOB = "32/03/2020";
        ProfilePage profilePage = new ProfilePage();
        profilePage.checkAndUpdateDOB(newDOB);
        profilePage.VerifyDOB();
    }*/

    /*    @Test
        public void test_loginFail() {
            LandingPage landingPage = new LandingPage();
            landingPage.clickOnLoginAndSignUpButton();

            LoginPage loginPage = new LoginPage();
            loginPage.login("0938765013", "123456");
            loginPage.getErrorText();
            String actuallyText = loginPage.getErrorText();
            Assert.assertEquals("Thông tin đăng nhập không đúng", actuallyText);
        }

        @Test
        public void test_userCanAccessProfilePage() {
            LandingPage landingPage = new LandingPage();
            landingPage.clickOnLoginAndSignUpButton();

            LoginPage loginPage = new LoginPage();
            loginPage.login("0938765013", "123456a@A");

            LandingPageAfterLoginSuccess landingPageAfterLoginSuccess = new LandingPageAfterLoginSuccess();
            landingPageAfterLoginSuccess.clickOnAccountSection();
            String checkURL = driver.getCurrentUrl();
            Assert.assertEquals("https://tiki.vn/customer/account/edit?src=header_my_account", checkURL);
        }

        @Test
        public void test_updateProfilePage_fullName() {
            LandingPage landingPage = new LandingPage();
            landingPage.clickOnLoginAndSignUpButton();

            LoginPage loginPage = new LoginPage();
            loginPage.login("0938765013", "123456a@A");

            LandingPageAfterLoginSuccess landingPageAfterLoginSuccess = new LandingPageAfterLoginSuccess();
            landingPageAfterLoginSuccess.clickOnAccountSection();

            //Please input a new value for full name field
            String newFullName = "Van Song Tran Nguyen";
            ProfilePage profilePage = new ProfilePage();
            profilePage.inputNewFullName(newFullName);

            String actuallyText = profilePage.getFullName();
            Assert.assertEquals(newFullName, actuallyText);
        }

        @Test
        public void test_updateProfilePage_checkRadio() {
            LandingPage landingPage = new LandingPage();
            landingPage.clickOnLoginAndSignUpButton();

            LoginPage loginPage = new LoginPage();
            loginPage.login("0938765013", "123456a@A");

            LandingPageAfterLoginSuccess landingPageAfterLoginSuccess = new LandingPageAfterLoginSuccess();
            landingPageAfterLoginSuccess.clickOnAccountSection();

            //Please select a Sex value and input: Sex.value Ex:Sex.Nam or Sex.Nữ
            ProfilePage profilePage = new ProfilePage();
            profilePage.checkAndVerifyBothSexRatio(Sex.Nữ);
        }

        @Test
        public void test_selectRandomProductByProductTitle() {
            LandingPage landingPage = new LandingPage();
            landingPage.clickOnLoginAndSignUpButton();

            LoginPage loginPage = new LoginPage();
            loginPage.login("0938765013", "123456a@A");

            CartPage cartPage = new CartPage();
            cartPage.checkAndClearCart();

            landingPage.scrollDownAndGetAllCurrentProduct(2000);
            landingPage.clickOnRandomProductTitle();

            ProductDetailPage productDetailPage = new ProductDetailPage();
            productDetailPage.purchaseProductAndClickOnCart();
            //https://tiki.vn/product-p91763297.html?spid=100867049
            //Cart button co luc nhan duoc luc khong StaleElementReferenceException: stale element reference: element is not attached to the page document
            cartPage.verifyCart();
            System.out.println("Step 1: Done!");
            //Try to check 2 product on List
            cartPage.returnHomePage();
            landingPage.scrollDownAndGetAllCurrentProduct(2000);
            landingPage.clickOnRandomProductTitle();
            cartPage.verifyCart();
        }

        @Test
        public void test_userCanUpdateDOB() {
            LandingPage landingPage = new LandingPage();
            landingPage.clickOnLoginAndSignUpButton();

            LoginPage loginPage = new LoginPage();
            loginPage.login("0938765013", "123456a@A");

            LandingPageAfterLoginSuccess landingPageAfterLoginSuccess = new LandingPageAfterLoginSuccess();
            landingPageAfterLoginSuccess.clickOnAccountSection();

            String newDOB = "28/02/2021";
            ProfilePage profilePage = new ProfilePage();
            profilePage.checkAndVerifyDOB(newDOB);
        }

        @Test
        public void test_userCanPickUpDateOnCalendar() {
            String inputDate = "02/05/2010";
            PickUpDateOnCalendar pickUpDate = new PickUpDateOnCalendar();
            pickUpDate.pickUpDate(inputDate);
        }*/
    @Test
    public static void getResponseBody() {
/*        given().queryParam("CUSTOMER_ID","68195")
                .queryParam("PASSWORD","1234!")
                .queryParam("Account_No","1")
                .when()
                .get("http://demo.guru99.com/V4/sinkministatement.php")
                .then()
                .log().body();*/
        Response response = RestAssured.get("https://reqres.in/api/users?page=2");
        System.out.println("Response: " + response.asString());
        System.out.println("Status code: " + response.getStatusCode());
        System.out.println("Body: " + response.getBody().asString());
        System.out.println("Time taken: " + response.getTime());
        System.out.println("Header: " + response.getHeader("content-type"));
    }
}
