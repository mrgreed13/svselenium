package com.company.entity;

import java.util.ArrayList;

public class Cart {
    private static ArrayList<Product> productOnCart = new ArrayList<>();
    //Tạo một private static instance của singleton class, đây chính là instance duy nhất của class này
    private static final Cart instance = new Cart();

    // hàm constructor để tránh các client-application sử dụng
    //contructor
    private Cart() {
    }

    public static ArrayList<Product> getProductObject()
    {
        return productOnCart;
    }

    public static Cart getInstance() {
        return instance;
    }

    public void addProductToCart(Product temp) {
        productOnCart.add(temp);
    }

    public int countProduct() {
        int count = productOnCart.size();
        return count;
    }

    public ArrayList<String> getTitle() {
        ArrayList<String> titleList = new ArrayList<>();
        String title = "";
        for (Product temp : productOnCart) {
            title = temp.getProductTitle();
            titleList.add(title);
        }
        return titleList;
    }

    public ArrayList<Double> getProductPrice() {
        ArrayList<Double> priceList = new ArrayList<>();
        for (Product temp : productOnCart) {
            double price = temp.getPrice();
            priceList.add(price);
        }
        return priceList;
    }
}
