package com.company.core.StepDefination;

import com.company.Pages.LandingPage;
import com.company.Pages.LandingPageAfterLoginSuccess;
import com.company.Pages.LoginPage;
import com.company.Pages.ProfilePage;
import com.company.Sex;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

public class ProfileStep {
    private final LandingPage landingPage = new LandingPage();
    private final LoginPage loginPage = new LoginPage();
    private final ProfilePage profilePage = new ProfilePage();
    private final LandingPageAfterLoginSuccess landingPageAfterLoginSuccess = new LandingPageAfterLoginSuccess();
    private final String newValidFullName = "Van Tran Nguyen";
    private final String blankFullName = " ";
    private final String invalidFullName = "Van";
    private final String invalidFullNameWithSixtyFiveLetter = "qưertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfg";
    private final String newDOB = "13/08/1993";

    @When("Logged in and access to Profile page")
    public void loggedInAndAccessToProfilePage() {
        landingPage.clickOnLoginAndSignUpButton();

        loginPage.loginWithValue("0938765013", "123456a@A");

        landingPageAfterLoginSuccess.clickOnAccountSection();
        String currentURL = landingPageAfterLoginSuccess.checkAccessSuccessful();
        Assert.assertEquals(currentURL, "https://tiki.vn/customer/account/edit?src=header_my_account");
    }

    @When("^Update full name with (valid|blank|invalid|invalid with sixty five letter) value$")
    public void updateFullName(String valueType) {
        switch (valueType) {
            case "valid":
                profilePage.inputNewFullName(newValidFullName);
                break;
            case "blank":
                profilePage.inputNewFullName(blankFullName);
                break;
            case "invalid":
                profilePage.inputNewFullName(invalidFullName);
                break;
            case "invalid with sixty five letter":
                profilePage.inputNewFullName(invalidFullNameWithSixtyFiveLetter);
                break;
        }
    }

    @Then("^Verify the current full name with (valid|blank|invalid|invalid with sixty five letter) value$")
    public void verifyTheCurrentFullName(String expectedValue) {
        String getFullName = profilePage.getFullName(false);
        switch (expectedValue) {
            case "valid":
                Assert.assertEquals(getFullName, newValidFullName);
                break;
            case "blank":
                Assert.assertEquals(getFullName, blankFullName);
                break;
            case "invalid":
                Assert.assertEquals(getFullName, invalidFullName);
                break;
            case "invalid with sixty five letter":
                Assert.assertEquals(getFullName, invalidFullNameWithSixtyFiveLetter);
                break;
        }
    }

    @And("^Show reminder message about (blank|invalid|invalid with sixty five letter) value$")
    public void showReminderMessage(String expectedValue) {
        String reminderMessage = profilePage.getReminderMessageAfterUpdate();
        switch (expectedValue) {
            case "blank":
                Assert.assertEquals(reminderMessage, "Vui lòng nhập họ tên");
                break;
            case "invalid":
                Assert.assertEquals(reminderMessage, "Tên phải chứa ít nhất 2 từ");
                break;
            case "invalid with sixty five letter":
                Assert.assertEquals(reminderMessage, "Độ dài tối đa cho \"họ tên\" là 64 kí tự");
                break;
        }
    }

    @And("^Show the (successful|failed) message$")
    public void showNotificationMessage(String expectedValue) {
        String notificationMessage;
        if (expectedValue.equals("successful")) {
            notificationMessage = profilePage.getNotificationMessage(expectedValue);
            //Assert.assertEquals(notificationMessage, "Thông tin tài khoản của bạn đã được cập nhật.");
            Assert.assertEquals(notificationMessage, "Cập nhật thông tin thành công");
        } else {
            notificationMessage = profilePage.getNotificationMessage(expectedValue);
            Assert.assertEquals(notificationMessage, "Cập nhật không thành công.");
        }
    }

    @And("^Refresh page and double check full name with (valid|blank|invalid|invalid with sixty five letter) value$")
    public void refreshPageAndDoubleCheckFullName(String expectedValue) {
        String actualFullName = profilePage.getFullName(true);
        switch (expectedValue) {
            case "valid":
                Assert.assertEquals(actualFullName, newValidFullName);
                break;
            case "blank":
                Assert.assertNotEquals(actualFullName, blankFullName);
                break;
            case "invalid":
                Assert.assertNotEquals(actualFullName, invalidFullName);
                break;
            case "invalid with sixty five letter":
                Assert.assertNotEquals(actualFullName, invalidFullNameWithSixtyFiveLetter);
                break;
        }
    }

    @When("^Select (male|female) gender$")
    public void selectMaleGender(String expectedGender) {
        if (expectedGender.equals("male")) {
            profilePage.selectSexRadio(Sex.Nam);
        } else if (expectedGender.equals("female")) {
            profilePage.selectSexRadio(Sex.Nữ);
        }
        else {
            profilePage.selectSexRadio(Sex.Khác);
        }
    }

    @Then("^Update (male|female) gender successful$")
    public void checkUpdateGender(String expectedGender) {
        switch (expectedGender) {
            case "male":
                Assert.assertTrue(profilePage.verifyChecked(Sex.Nam, "no"));
                break;
            case "female":
                Assert.assertTrue(profilePage.verifyChecked(Sex.Nữ, "no"));
                break;
            case "other":
                Assert.assertTrue(profilePage.verifyChecked(Sex.Khác, "no"));
                break;
        }
    }

    @And("^Refresh page and double check (male|female|other) gender is selected$")
    public void refreshPageAndDoubleCheckMaleGenderIsSelected(String expectedGender) {
        switch (expectedGender) {
            case "male":
                Assert.assertTrue(profilePage.verifyChecked(Sex.Nam, "yes"));
                break;
            case "female":
                Assert.assertTrue(profilePage.verifyChecked(Sex.Nữ, "yes"));
                break;
            case "other":
                Assert.assertTrue(profilePage.verifyChecked(Sex.Nữ, "yes"));
                break;
        }

    }

    @When("Select new DOB")
    public void selectNewDOB() {
        profilePage.checkAndInputNewDOB(newDOB);
    }

    @Then("^(First time|Double) check DOB update is successful$")
    public void updateDOBSuccessful(String expectedValue) {
        if (expectedValue.equals("First time")) {
            String actualDOB = profilePage.getDOB(false);
            Assert.assertEquals(actualDOB, newDOB);
        } else {
            String actualDOB = profilePage.getDOB(true);
            Assert.assertEquals(actualDOB, newDOB);
        }
    }


}
