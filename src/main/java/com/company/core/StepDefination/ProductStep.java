package com.company.core.StepDefination;

import com.company.Pages.*;
import com.company.entity.Cart;
import com.company.entity.Product;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;

public class ProductStep {
    private final LandingPage landingPage = new LandingPage();
    private final LoginPage loginPage = new LoginPage();
    private final LandingPageAfterLoginSuccess landingPageAfterLoginSuccess = new LandingPageAfterLoginSuccess();
    private final ProductDetailPage productDetailPage = new ProductDetailPage();
    private final CartPage cartPage = new CartPage();

    @When("Login and clear Cart if any then return to Landing page")
    public void loginThenComeBackToLandingPage() {
        landingPage.clickOnLoginAndSignUpButton();
        loginPage.loginWithValue("0938765013", "123456a@A");
        cartPage.checkAndClearCart();
    }

    @When("Select random product")
    public void selectRandomProduct() {
        landingPage.scrollDownAndGetAllCurrentProduct(2000);
        landingPage.clickOnRandomProductTitle();
        productDetailPage.purchaseProductAndClickOnCart();
    }

    @Then("Check product on Cart")
    public void checkProductOnCart() {
/*        //old assert
        cartPage.verifyCart();*/

        //try the new assert
        List<WebElement> getTitleAndQuantityList = cartPage.getTitleAndQuantity();
        List<WebElement> getPriceList = cartPage.getPrice();
        if (getTitleAndQuantityList.size() > 0) {
            //verifyQuantityOfProduct
            Waiter.wait(1);
            Assert.assertEquals(getTitleAndQuantityList.size(), Cart.getInstance().countProduct());

            //Declare List product object & tempObj product object
            List<Product> productsOnUI = new ArrayList<>();
            Product tempObj = new Product();

            //Add product title and product price for Product on UI List
            for (int i = 0; i < getTitleAndQuantityList.size(); i++) {
                tempObj.setProductTitle(getTitleAndQuantityList.get(i).getText());

                String priceActualStr = getPriceList.get(i).getText();
                priceActualStr = priceActualStr.replaceAll("đ", "");
                priceActualStr = priceActualStr.replace(".", "");
                double priceActual = Double.parseDouble(priceActualStr);
                tempObj.setPrice(priceActual);

                productsOnUI.add(tempObj);
            }
            //Get List of product on Cart to compare with List of product on UI
            List<Product> productsOnCart = Cart.getProductObject();
            for (int i = 0; i < getTitleAndQuantityList.size(); i++) {
                //verifyTitleOfProduct
                String actualTitle = productsOnUI.get(i).getProductTitle();
                String expectedTitle = productsOnCart.get(i).getProductTitle();
                String[] actualTitleSplit = actualTitle.split(" - ");
                String[] expectedTitleSplit = expectedTitle.split(" - ");
                Assert.assertEquals(actualTitleSplit[0], expectedTitleSplit[0]);
                //Assert.assertEquals(productsOnUI.get(i).getProductTitle(), productsOnCart.get(i).getProductTitle());

                //verifyPriceOfProduct
                Assert.assertEquals(productsOnUI.get(i).getPrice(), productsOnCart.get(i).getPrice());
            }
        }
    }
}
