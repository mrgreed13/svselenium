package com.company.core.StepDefination;

import com.company.Pages.LandingPage;
import com.company.Pages.LandingPageAfterLoginSuccess;
import com.company.Pages.LoginPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

public class LoginStep {
    private final LandingPage landingPage = new LandingPage();
    private final LoginPage loginPage = new LoginPage();

    @When("Start Tiki and go to login page")
    public void login() {
        //LandingPage landingPage = new LandingPage();
        landingPage.clickOnLoginAndSignUpButton();
    }

    @When("Login with valid account")
    public void loginWithValidAccount() {
        //LoginPage loginPage = new LoginPage();
        loginPage.loginWithValue("0938765013", "123456a@A");
    }

    @When("^Login with invalid account - (blank|invalid|unregistered|unregistered and out of verifying limit) id$")
    public void loginWithInvalidID(String isValid) {
        //LoginPage loginPage = new LoginPage();
        switch (isValid) {
            case "blank":
                loginPage.loginWithValue("", "");
                break;
            case "invalid":
                loginPage.loginWithValue("093876501", "");
                break;
            case "unregistered":
                loginPage.loginWithValue("0931111112", "");
                break;
            case "unregistered and out of verifying limit":
                loginPage.loginWithValue("0931111111", "");
                break;
        }
    }

    @When("^Login with invalid account - (blank|invalid) password$")
    public void loginWithInvalidPassword(String isValid) {
        //LoginPage loginPage = new LoginPage();
        switch (isValid) {
            case "blank":
                loginPage.loginWithValue("0938765013", "");
                break;
            case "invalid":
                loginPage.loginWithValue("0938765013", "123");
                break;
        }
    }

/*
    @Then("^Login successful$")
    public void loginSuccessful() {
        String actualText = loginPage.checkLoginResult();
        if (actualText.trim().isEmpty()) {
            System.out.println("Can not get actual text. Please check again!");
        } else {
            Assert.assertEquals(actualText, "Tài Khoản");
        }
    }

    @Then("^Login failed with (blank id|invalid id|unregistered id|unregistered id and out of verifying limit|blank password|invalid password)$")
    public void loginFailed(String kindOfInvalid) {
        String actualText = loginPage.checkLoginResult();
        if (actualText.trim().isEmpty()) {
            System.out.println("Can not get actual text. Please check again!");
        } else {
            switch (kindOfInvalid) {
                case "blank id":
                    Assert.assertEquals(actualText, "Số điện thoại không được để trống");
                    break;
                case "invalid id":
                    Assert.assertEquals(actualText, "Số điện thoại không đúng định dạng.");
                    break;
                case "unregistered id":
                    Assert.assertEquals(actualText, "Nhập mã xác minh");
                    break;
                case "unregistered id and out of verifying limit":
                    Assert.assertEquals(actualText, "Mã xác thực chỉ được gửi tối đa 5 lần trong một ngày.");
                    break;
                case "blank password":
                    Assert.assertEquals(actualText, "Mật khẩu không được để trống");
                    break;
                case "invalid password":
                    Assert.assertEquals(actualText, "Thông tin đăng nhập không đúng");
                    break;
            }
        }
    }
*/

    @Then("^Login (successful|failed) with (valid id|blank id|invalid id|unregistered id|unregistered id and out of verifying limit|blank password|invalid password)$")
    public void showLoginResult(String kindOfResult, String kindOfInvalid) {
        String actualText = loginPage.checkLoginResult();
        if (actualText.trim().isEmpty()) {
            System.out.println("Can not get actual text. Please check again!");
        } else {
            if (kindOfResult.equals("successful") && kindOfInvalid.equals("valid id")) {
                Assert.assertEquals(actualText, "Tài Khoản");
            }
            else {
                switch (kindOfInvalid) {
                    case "blank id":
                        Assert.assertEquals(actualText, "Số điện thoại không được để trống");
                        break;
                    case "invalid id":
                        Assert.assertEquals(actualText, "Số điện thoại không đúng định dạng");
                        break;
                    case "unregistered id":
                        Assert.assertEquals(actualText, "Nhập mã xác minh");
                        break;
                    case "unregistered id and out of verifying limit":
                        Assert.assertEquals(actualText, "Mã xác thực chỉ được gửi tối đa 5 lần trong một ngày.");
                        break;
                    case "blank password":
                        Assert.assertEquals(actualText, "Mật khẩu không được để trống");
                        break;
                    case "invalid password":
                        Assert.assertEquals(actualText, "Thông tin đăng nhập không đúng");
                        break;
                }
            }
        }
    }


}
