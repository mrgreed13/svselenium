package com.company.core.seleniumFramework;

import com.company.Pages.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Button extends BaseElement {

    private static String type = "Button";
    private int attempts = 0;
    private int time = 2;

/*    public Button(By by, String type, String name) {
        super(by, type, name);
    }*/

    public Button(By by, String name) {
        super(by, name);
    }

    public void waitForClickable() {
        while (attempts < time) {
            try {
                Waiter.waitForElementToBeClickable(super.getElement());
                break;
            } catch (StaleElementReferenceException e) {
                e.getMessage();
            }
            attempts++;
        }
    }

    public boolean isButtonClickable() {
        return (getElement().isDisplayed() && getElement().isEnabled());
    }
}
