package com.company.core.seleniumFramework;

import com.company.Pages.Waiter;
import com.company.core.BaseEntity;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Wait;

public abstract class BaseElement extends BaseEntity {
    private int attempts = 0;
    private int time = 2;
    private By by;
    private String elementName;
    private String elementType;

/*    public BaseElement(By by, String type, String name) {
        this.by = by;
        this.elementType = type;
        this.elementName = name;
    }*/

    public BaseElement(By by, String name) {
        this.by = by;
        this.elementName = name;
    }

    protected WebElement getElement() {
        WebElement webElement = null;
        while (attempts < time) {
            try {
                webElement = driver.findElement(by);
                break;
            } catch (StaleElementReferenceException e) {
                e.getMessage();
            }
            attempts++;
        }
        return webElement;
    }

    public Boolean isElementDisplay() {
        boolean flag = false;
        while (attempts < time) {
            try {
                flag = driver.findElement(by).isDisplayed();
                break;
            } catch (NoSuchElementException e) {
                return false;
            } catch (StaleElementReferenceException e) {
                e.getMessage();
            }
            attempts++;
        }
        return flag;
    }

/*    public Boolean isElementPresent() {
        boolean flag = false;
        while (attempts < time) {
            try {
                flag = driver.findElement(by).size() != 0;
                break;
            } catch (NoSuchElementException e) {
                return false;
            } catch (StaleElementReferenceException e) {
                e.getMessage();
            }
            attempts++;
        }
        return flag;
    }*/

    public Boolean isElementDisappear() {
        try {
            WebElement element = driver.findElement(by);
            return !element.isDisplayed();
        } catch (NoSuchElementException e) {
            return true;
        }
    }

    public void waitForElementToBeDisplay() {
        while (attempts < time) {
            try {
                Waiter.waitForElementToBeDisplay(driver.findElement(by));
                break;
            } catch (StaleElementReferenceException | NoSuchElementException e) {
                e.getMessage();
            }
            attempts++;
        }
    }

    public void waitForElementToBeEnable() {
        Waiter.waitForElementToBeEnable(driver.findElement(by));
    }

    public void waitForPresenceOfElement() {
        Waiter.waitForPresenceOfElement(by);
    }

    public void waitForElementToBeDisappear() {
        try {
            Waiter.waitForElementToBeDisappear(driver.findElement(by));
        } catch (NoSuchElementException e) {

        }
    }

    public void click() {
        while (attempts < time) {
            try {
                driver.findElement(by).click();
                break;
            } catch (StaleElementReferenceException e) {
                e.getMessage();
            }
            attempts++;
        }
    }

    public void clickViaJS() {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", driver.findElement(by));
    }

    public void hoverMouse() {
        Actions actions = new Actions(driver);
        while (attempts < time) {
            attempts++;
        }
    }


}
