package com.company.core.seleniumFramework;

import com.company.Pages.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;

public class TextBox extends BaseElement {
    private static String type = "TextBox";
    private int attempts = 0;
    private int time = 2;


/*    public TextBox(By by, String type, String name) {
        super(by, type, name);
    }*/

    public TextBox(By by, String name) {
        super(by, name);
    }

    public void sendClearText(String text) {
        while (attempts < time) {
            try {
                super.getElement().clear();
                Thread.sleep(500);
                super.getElement().sendKeys(text);
                break;
            } catch (StaleElementReferenceException | InterruptedException e) {
                e.printStackTrace();
            }
            attempts++;
        }
    }

    public void waitForTextBoxToBePresent(String text) {
        Waiter.waitForTextPresent(super.getElement(), text);
    }

/*    public void sendClearTextViaJS(String text) {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript(String.format("arguement[0].setAttribute('value','%s'):", text), driver.findElement(super.getBy()));
    }*/

    public void sendTextIntoValueAttribute(String text) {
        while (attempts < time) {
            try {
                super.getElement().clear();
                Thread.sleep(500);
                super.getElement().sendKeys("value", text);
                break;
            } catch (StaleElementReferenceException | InterruptedException e) {
                e.printStackTrace();
            }
            attempts++;
        }
    }
}
