package com.company.core;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.cucumber.core.gherkin.Scenario;
import org.apache.commons.exec.OS;
import org.openqa.selenium.WebDriver;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public abstract class BaseEntity {
    protected static WebDriver driver;

    private final static String[] seleniumBrower = new String[]{"CHROME", "FIREFOX"};
    private final static String[] Environment = new String[]{"DEV", "PROD"};
    private final static String[] Language = new String[]{"EN", "VI"};
    private static Scenario scenario;

    public static Properties getEnvironmentSetting() {
        String environment = System.getProperty("environment");
        final Properties properties = new Properties();

        if (environment.equals("prod")) {
            try {
                BufferedReader ip = new BufferedReader(new InputStreamReader(new FileInputStream(PathFinder.getEnvironment("prod")), StandardCharsets.UTF_8));
                properties.load(ip);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                BufferedReader ip = new BufferedReader(new InputStreamReader(new FileInputStream(PathFinder.getEnvironment("dev")), StandardCharsets.UTF_8));
                properties.load(ip);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return properties;
    }

    public static OS getOS() {
        String os = System.getProperty("os.name").toLowerCase();
        System.out.println("Current OS: " + os);
        if (os.contains("windows")) {
            return OS.WINDOWS;
        }
        else if (os.startsWith("mac")) {
            return OS.MAC;
        }
        else {
            return OS.LINUX;
        }
    }

    protected enum OS {
        WINDOWS,
        MAC,
        LINUX
    }
}
