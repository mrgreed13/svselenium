package com.company.core;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;

public class BrowserFactory extends BaseEntity {
    static String browser = System.getProperty("browser");

    public static void initBrowser() {
        if (browser.equals("chrome")) {
            //System.setProperty("webdriver.chrome.driver", pathToRootDirectory + File.separator + "drivers" + File.separator + "mac_chromedriver");
            //System.setProperty("webdriver.chrome.driver", getRootDirectory() + File.separator + "drivers" + File.separator + "win_chromedriver.exe");
            System.setProperty("webdriver.chrome.driver", PathFinder.getDriver("chrome"));
            driver = new ChromeDriver();
        } else if (browser.equals("firefox")) {
            //System.setProperty("webdriver.gecko.driver", getRootDirectory() + File.separator + "drivers" + File.separator + "win_geckodriver.exe");
            System.setProperty("webdriver.gecko.driver", PathFinder.getDriver("firefox"));
            driver = new FirefoxDriver();
        } else {
            //System.setProperty("webdriver.edge.driver", getRootDirectory() + File.separator + "drivers" + File.separator + "win_msedgedriver.exe");
            System.setProperty("webdriver.edge.driver", PathFinder.getDriver("edge"));
            driver = new EdgeDriver();
        }
    }
}
