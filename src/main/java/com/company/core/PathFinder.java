package com.company.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class PathFinder {
    public static String getRootDirectory() {
        String pathToRootDirectory = System.getProperty("user.dir");
        return pathToRootDirectory;
    }

    public static String getEnvironment(String envName) {
        String outputPath = getRootDirectory() + File.separator + "config" + File.separator;
        switch (envName) {
            case "prod":
                outputPath += "prod.properties";
                break;
            case "dev":
                outputPath += "dev.properties";
                break;
            default:
                System.out.println("Cannot get environment path!");
                break;
        }
        return outputPath;
    }
    public static String getDriver(String driverName) {
        String outputPath = getRootDirectory() + File.separator + "drivers" + File.separator;
        switch (driverName) {
            case "chrome":
                outputPath += "win_chromedriver.exe";
                break;
            case "firefox":
                outputPath += "win_geckodriver.exe";
                break;
            case "edge":
                outputPath += "win_msedgedriver.exe";
                break;
            default:
                System.out.println("Cannot get browser path!");
                break;
        }
        return outputPath;
    }
}
