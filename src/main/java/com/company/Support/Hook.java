package com.company.Support;

import com.company.Pages.BasePage;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.IOException;

public class Hook extends BasePage {
    @Before
    public void before(Scenario scenario) throws IOException {
        setupAndClickOnSkip();
    }

    @After
    public void after(Scenario scenario) {
        if (scenario.isFailed()) {
            final byte[] screenShot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenShot, "image/png", scenario.getName());
        }
        tearDown();
    }
}
