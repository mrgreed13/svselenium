package com.company.Pages;

import com.company.core.mobileBaseEntity;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class mobileHomePlatformPage extends mobileBasePage{
    private final String cornerStoneBtn = "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button";
    private final String laterBtn = "//android.widget.TextView[@content-desc=\"Để sau/Text\"]";
    //private final String searchIc = "//android.widget.ImageView[@content-desc=\"iconSearch\"]";
    private final String searchIc = "//android.widget.ImageView[@content-desc=\"iconNoti\"]";
    //private final String cancelSaveYourInfoPopup = "//android.widget.TextView[@content-desc=\"Để sau/Text\"]";

    private final String momoIconAtNavBar = "//android.widget.Button[@content-desc=\"MOMO, tab, 1 of 6\"]";
    private final String uuDaiIconAtNavBar = "//android.widget.Button[@content-desc=\"ƯU ĐÃI, tab, 2 of 6\"]";
    private final String lsgdIconAtNavBar = "//android.widget.Button[@content-desc=\"LỊCH SỬ GD, tab, 3 of 6\"]";
    private final String chatIconAtNavBar = "//android.widget.Button[@content-desc=\"CHAT, tab, 5 of 6\"]";
    private final String accountIconAtNavBar = "//android.widget.Button[@content-desc=\"VÍ CỦA TÔI, tab, 6 of 6\"]";
    public void closePopup() {
//        mobileWaiter.waitForElementToBeDisplay(cornerStoneBtn);
//        MobileElement cornerStoneButton = anDroid.findElement(By.xpath(cornerStoneBtn));
//        cornerStoneButton.click();

        mobileWaiter.waitForElementToBeDisplay(laterBtn);
        MobileElement laterButton = mobileBaseEntity.androidDriver.findElement(By.xpath(laterBtn));
        laterButton.click();
    }

    public void accessSearchPage() {
        mobileWaiter.waitForElementToBeDisplay(searchIc);
        MobileElement searchIcon = mobileBaseEntity.androidDriver.findElement(By.xpath(searchIc));
        searchIcon.click();
    }
}
