package com.company.Pages;

import com.company.core.seleniumFramework.Button;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class LandingPage extends BasePage {
    //private final WebElement buttonLoginAndSignUp = driver.findElement(By.xpath("//span[text()='Đăng Nhập / Đăng Ký']"));
    private final Random randomNo = new Random();
    private int rdNo = 0;

    //locators
    private final String xpath_buttonLoginAndSignUp = "//span[text()='Đăng Nhập / Đăng Ký']";

    //elements
    private final Button buttonLoginAndSignUp = new Button(By.xpath(xpath_buttonLoginAndSignUp), "Login And SignUp");

    public void clickOnLoginAndSignUpButton() {
        buttonLoginAndSignUp.click();
        //WebElement buttonLogin = driver.findElement(By.xpath("//button[text()='Đăng nhập']"));
        //buttonLogin.click();
        Waiter.wait(1);
    }

    public void scrollDownAndGetAllCurrentProduct(int pixel) {
        JsScroll.srollDown(pixel);
        Waiter.wait(2);
    }

    public void clickOnRandomProductTitle() {
        //List dang dung thuan Selen
        List<WebElement> productList = driver.findElements(By.xpath("//a[@class='product-item']//div[@class='name']"));
        rdNo = randomNo.nextInt(productList.size());
        productList.get(rdNo).click();
    }

/*    public void setEngToDefaultLanguage() {
        WebElement changeLanguageBtn = driver.findElement(By.xpath("//div[@id='topActionSwitchLang']/span"));
        changeLanguageBtn.click();
        Waiter.wait(1);
        if (isElementPresent(By.xpath("//div[@data-lang='vi' and @class='lzd-switch-item currentSelected']"))) {
            WebElement engLanguage = driver.findElement(By.xpath("//div[@data-lang='en' and @class='lzd-switch-item ']"));
            engLanguage.click();
            Waiter.wait(1);
        }
    }*/

    public void selectRemainingButton() {
        WebElement remainingLanguage = driver.findElement(By.xpath("//div[@class='lzd-switch-item ']"));
        remainingLanguage.click();
        Waiter.wait(1);
    }
}
