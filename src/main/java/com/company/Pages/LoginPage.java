package com.company.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class LoginPage extends BasePage {
    private final String buttonNextLocator = "//button[text()='Tiếp Tục']";
    //private final WebElement textBoxUserName = driver.findElement(By.xpath("//input[@placeholder='Số điện thoại']"));
    //private final WebElement buttonNext = driver.findElement(By.xpath(buttonNextLocator));
    String verifyText = "";
    private int checkLoginCase;


    public void loginWithValue(String username, String password) {
        Waiter.wait(1);
        Waiter.waitForElementToBeDisplay(driver.findElement(By.xpath("//input[@placeholder='Số điện thoại']")));
        WebElement textBoxUserName = driver.findElement(By.xpath("//input[@placeholder='Số điện thoại']"));
        textBoxUserName.sendKeys(username);
        WebElement buttonNext = driver.findElement(By.xpath(buttonNextLocator));
        buttonNext.click();
        Waiter.wait(2);
        if (isElementPresent(By.xpath("//span[text()='Số điện thoại không được để trống']"))) {
            checkLoginCase = 1;
            //System.out.println("1");
        } else if (isElementPresent(By.xpath("//span[text()='Số điện thoại không đúng định dạng']"))) {
            checkLoginCase = 2;
            //System.out.println("2");
        } else if (isElementPresent(By.xpath("//h4[text()='Nhập mã xác minh']"))) {
            checkLoginCase = 3;
            //System.out.println("3");
        } else if (isElementPresent(By.xpath("//span[text()='Mã xác thực chỉ được gửi tối đa 5 lần trong một ngày.']"))) {
            checkLoginCase = 4;
            //System.out.println("4");
        } else {
            //Waiter.wait(5);
            Waiter.waitForElementToBeDisplay(driver.findElement(By.xpath("//input[@placeholder='Mật khẩu']")));
            WebElement texBoxPassword = driver.findElement(By.xpath("//input[@placeholder='Mật khẩu']"));
            texBoxPassword.sendKeys(password);
            WebElement buttonLogin = driver.findElement(By.xpath("//button[text()='Đăng Nhập']"));
            buttonLogin.click();
            Waiter.wait(2);
            if (isElementPresent(By.xpath("//span[text()='Mật khẩu không được để trống']"))) {
                checkLoginCase = 5;
                Waiter.wait(1);
                //System.out.println("5");
            } else if (isElementPresent(By.xpath("//span[text()='Thông tin đăng nhập không đúng']"))) {
                checkLoginCase = 6;
                Waiter.wait(1);
                //System.out.println("6");
            } else {
                checkLoginCase = 7;
                Waiter.wait(2);
                Waiter.waitForElementToBeDisappear(buttonLogin);
                //System.out.println("7");
            }
        }
    }

    public String checkLoginResult() {
        String actualText = null;
        switch (checkLoginCase) {
            case 1:
                WebElement blankErrorMess = driver.findElement(By.xpath("//span[text()='Số điện thoại không được để trống']"));
                actualText = blankErrorMess.getText();
                //Assert.assertEquals(blankErrorMess.getText(), "Số điện thoại không được để trống");
                //System.out.println("1");
                break;
            case 2:
                //Waiter.waitForElementToBeDisplay(driver.findElement(By.xpath("//span[text()='Số điện thoại không đúng định dạng']")));
                WebElement invalidErrorMess = driver.findElement(By.xpath("//span[text()='Số điện thoại không đúng định dạng']"));
                actualText = invalidErrorMess.getText();
                //Assert.assertEquals(invalidErrorMess.getText(), "Số điện thoại không đúng định dạng.");
                //System.out.println("2");
                break;
            case 3:
                WebElement unregisteredUsername = driver.findElement(By.xpath("//h4[text()='Nhập mã xác minh']"));
                Waiter.waitForElementToBeDisplay(unregisteredUsername);
                actualText = unregisteredUsername.getText();
                //Assert.assertEquals(unregisteredUsername.getText(), "Nhập mã xác minh");
                //System.out.println("3");
                break;
            case 4:
                WebElement unregisteredUsernameAndOutOfVerifyingLimit = driver.findElement(By.xpath("//span[text()='Mã xác thực chỉ được gửi tối đa 5 lần trong một ngày.']"));
                Waiter.waitForElementToBeDisplay(unregisteredUsernameAndOutOfVerifyingLimit);
                actualText = unregisteredUsernameAndOutOfVerifyingLimit.getText();
                //Assert.assertEquals(unregisteredUsernameAndOutOfVerifyingLimit.getText(), "Mã xác thực chỉ được gửi tối đa 5 lần trong một ngày.");
                //System.out.println("4");
                break;
            case 5:
                WebElement blankPassword = driver.findElement(By.xpath("//span[text()='Mật khẩu không được để trống']"));
                actualText = blankPassword.getText();
                //Assert.assertEquals(blankPassword.getText(), "Mật khẩu không được để trống");
                //System.out.println("5");
                break;
            case 6:
                WebElement invalidPassword = driver.findElement(By.xpath("//span[text()='Thông tin đăng nhập không đúng']"));
                actualText = invalidPassword.getText();
                //Assert.assertEquals(invalidPassword.getText(), "Thông tin đăng nhập không đúng");
                //System.out.println("6");
                break;
            case 7:

                driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
                WebElement verifyLoginSuccessful = driver.findElement(By.xpath("//span[text()='Tài Khoản']"));
                actualText = verifyLoginSuccessful.getText();
                //Assert.assertEquals(verifyLoginSuccessful.getText(), "Tài Khoản");
                //System.out.println("7");
                break;
        }
        return actualText;
    }

    public void login(String username, String password) {
        // input phone number then next
        WebElement textBoxUserName = driver.findElement(By.xpath("//input[@placeholder='Số điện thoại']"));
        Waiter.waitForElementToBeDisplay(textBoxUserName);
        textBoxUserName.sendKeys(username);
        WebElement buttonNext = driver.findElement(By.xpath(buttonNextLocator));
        buttonNext.click();
        Waiter.wait(1);
        if (isElementPresent(By.xpath("//span[text()='Số điện thoại không được để trống']"))) {
            WebElement blankErrorMess = driver.findElement(By.xpath("//span[text()='Số điện thoại không được để trống']"));
            Assert.assertEquals(blankErrorMess.getText(), "Số điện thoại không được để trống");
        } else if (isElementPresent(By.xpath("//span[text()='Số điện thoại không đúng định dạng. ']"))) {
            WebElement blankErrorMess = driver.findElement(By.xpath("//span[text()='Số điện thoại không đúng định dạng. ']"));
            Assert.assertEquals(blankErrorMess.getText(), "Số điện thoại không đúng định dạng. ");
        } else if (isElementPresent(By.xpath("//h4[text()='Nhập mã xác minh']"))) {
            WebElement invalidErrorMess = driver.findElement(By.xpath("//h4[text()='Nhập mã xác minh']"));
            Assert.assertEquals(invalidErrorMess.getText(), "Nhập mã xác minh");
        } else {
            Waiter.waitForElementToBeDisplay(driver.findElement(By.xpath("//input[@placeholder='Mật khẩu']")));
            WebElement texBoxPassword = driver.findElement(By.xpath("//input[@placeholder='Mật khẩu']"));
            texBoxPassword.sendKeys(password);
            Waiter.waitForElementToBeDisplay(driver.findElement(By.xpath("//button[text()='Đăng Nhập']")));
            WebElement buttonLogin = driver.findElement(By.xpath("//button[text()='Đăng Nhập']"));
            buttonLogin.click();

            if (isElementPresent(By.xpath("//span[text()='Mật khẩu không được để trống']"))) {
                WebElement blankPassword = driver.findElement(By.xpath("//span[@class='error-mess']"));
                Assert.assertEquals(blankPassword.getText(), "Mật khẩu không được để trống");
            } else if (isElementPresent(By.xpath("//span[text()='Thông tin đăng nhập không đúng']"))) {
                WebElement invalidPassword = driver.findElement(By.xpath("//span[text()='Thông tin đăng nhập không đúng']"));
                Assert.assertEquals(invalidPassword.getText(), "Thông tin đăng nhập không đúng");
            } else {
                Waiter.waitForElementToBeDisappear(buttonLogin);
                Waiter.wait(1);
                driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
                WebElement verifyLoginSuccessful = driver.findElement(By.xpath("//span[text()='Tài Khoản']"));
                Waiter.waitForElementToBeDisplay(verifyLoginSuccessful);
                Assert.assertEquals(verifyLoginSuccessful.getText(), "Tài Khoản");
            }
        }
    }

    public String getErrorText() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement getText = driver.findElement(By.xpath("//span[text()='Thông tin đăng nhập không đúng']"));
        Waiter.waitForElementToBeDisplay(getText);
        return getText.getText();
    }

    public String toString() {
        return verifyText;
    }
}
