package com.company.Pages;

import com.company.core.mobileBaseEntity;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.By;

import java.util.List;

public class mobileTDMMHomePage extends mobileBasePage {
    //private final String closeLocationIcon = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.ImageView";
    private final String closeLocationIcon = "//*[starts-with(@text, 'Cho phép truy cập Vị')]/following-sibling::android.view.ViewGroup[1]";
    private final String nextBtn = "//*[starts-with(@text, 'Tiếp tụ')]";

    private final String defaultLocation = "//android.widget.TextView[@content-desc=\"Quận 1, Phố đi bộ Nguyễn Huệ/Text\"]";

    //android.widget.TextView[@text="Cho phép truy cập Vị trí/Text"]
    //*[starts-with(@text, 'Cho phép truy cập Vị')]//following::android.widget.ImageView[0]

    private final String homeItems = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/androidx.viewpager.widget.ViewPager/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup";

    private final String firstOAItem = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/androidx.viewpager.widget.ViewPager/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]";

    private String oaTitle = "";
    private final String firstOAName = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.FrameLayout/androidx.viewpager.widget.ViewPager/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]//child::android.widget.TextView[1]";
    //Home
    private final String oaImage = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.FrameLayout/androidx.viewpager.widget.ViewPager/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.HorizontalScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ImageView";
    //Nav Bar
    private final String accountIconOnNavBar = "//android.widget.TextView[@content-desc=\"Tôi/Text\"]";
    private final String navBottomBar = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup";
    //private static String navBottomBar;
    private String wishIcon = "";

    public void closeLoactionPopup() {
        mobileWaiter.waitForElementToBeDisplay(closeLocationIcon);
        MobileElement closeLoactionPopupIcon = mobileBaseEntity.androidDriver.findElement(By.xpath(closeLocationIcon));
        closeLoactionPopupIcon.click();
    }

    public String getDefaultLocation() {
        mobileWaiter.waitForElementToBeDisplay(defaultLocation);
        MobileElement defaultLocationField = mobileBaseEntity.androidDriver.findElement(By.xpath(defaultLocation));
        return defaultLocationField.getText();
    }

    public List<AndroidElement> getHomeIndex() {
        mobileWaiter.waitForElementToBeDisplay(homeItems);
        List<AndroidElement> valueOfHomeItems = mobileBaseEntity.androidDriver.findElements(By.xpath(homeItems));
        return valueOfHomeItems;
    }

    public String getTheFirstOAName() {
        mobileWaiter.waitForElementToBeDisplay(firstOAName);
        MobileElement firstOATitle = mobileBaseEntity.androidDriver.findElement(By.xpath(firstOAName));
        return firstOATitle.getText();
    }

    public void tapOnWishIcon(String navIcon) {
        if (navIcon.equals("Thổ địa")) {
            wishIcon = navBottomBar + "//descendant::android.widget.TextView[@content-desc=\"Thổ địa/Text\"]";
        }
        else if (navIcon.equals("Ưu đãi xịn")) {
            wishIcon = navBottomBar + "//descendant::android.widget.TextView[@content-desc=\"Ưu đãi xịn/Text\"]";
        }
        else if (navIcon.equals("Bộ sưu tập")) {
            wishIcon = navBottomBar + "//descendant::android.widget.TextView[@content-desc=\"Bộ sưu tập/Text\"]";
        }
        else if (navIcon.equals("Tôi")) {
            //wishIcon = navBottomBar + "//android.view.ViewGroup[4]//android.widget.TextView[@content-desc=\"Tôi/Text\"]";
            wishIcon = "//descendant::android.widget.TextView[@content-desc=\"Tôi/Text\"]";
        }
        else {
            System.out.println("Cannot get your wish icon xpath");
        }
        mobileWaiter.waitForElementToBeDisplay(wishIcon);
        MobileElement yourWishIcon = mobileBaseEntity.androidDriver.findElement(By.xpath(wishIcon));
        yourWishIcon.click();

    }
}