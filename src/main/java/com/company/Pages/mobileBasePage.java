package com.company.Pages;

import com.company.core.mobileBaseEntity;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import io.restassured.RestAssured;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.net.MalformedURLException;
import java.net.URL;

public class mobileBasePage extends mobileBaseEntity {

    private static final String developmentMoMoAppPackage = "vn.momo.platform.test";
    private static final String developmentMoMoAppAcitvity = "vn.momo.platform.MainActivity";
    private static final String appiumHost = "http://127.0.0.1:4723/wd/hub";
    private static final String endpointOnDev = "http://api-dev.mservice.io";
    private static final String pathOnDev = "/routerx-internal-app-api-merchant";
    public static final String userWallet = "0938765013";
    public static final String loginOTP = "0000";
    public static final String userPassword = "000000";
    public static final String tdmmKeyWord = "Thổ địa MoMo";
    public static final String defaultLocation = "Quận 1, Phố đi bộ Nguyễn Huệ";

    public static final String accountTitle = "Trang cá nhân";
    public static final String bstCuaToiCTA = "Bộ sưu tập của tôi";
    public static final String searchPlaceHolder = "Tìm kiếm bộ sưu tập của bạn";
    public static final String defaultCollection = "Yêu thích";
    public static final String tatCaBtn = "Tất cả";
    public static final String cuaToiBtn = "Của tôi";
    public static final String topTrendingBtn = "Top Trending";
    public static final String taoBSTMoiTitle = "Tạo bộ sưu tập mới";
    public static final String inputTitle = "Tên bộ sưu tập";
    public static final String countLetterNumber = "(0/48)";
    public static final String inputPlaceHolder = "Bạn thích đặt tên gì?";
    public static final String createBtn = "Tạo";
    public static final String newCollectionName = "BST mới nè 2";



/*    @Before
    public void init() {
        RestAssured.baseURI = "http://api-dev.mservice.io/api";
        RestAssured.basePath = "/routerx-internal-app-api-merchant";
        //RestAssured.port = 8080;
    }*/
    @BeforeTest
    public void accessMiniApp() throws MalformedURLException {
        caps = new DesiredCapabilities();
        caps.setCapability(MobileCapabilityType.DEVICE_NAME, "R58M73SWCWY");
        caps.setCapability("platformName", "android");
        caps.setCapability("appPackage", developmentMoMoAppPackage);
        caps.setCapability("appActivity", developmentMoMoAppAcitvity);
        caps.setCapability("newCommandTimeout", 60*2);

        androidDriver = new AndroidDriver<>(new URL(appiumHost), caps);
        try {
            Thread.sleep(3000);
        } catch (Exception ign) {}
        //not good for practice
        RestAssured.baseURI = endpointOnDev;
        RestAssured.basePath = pathOnDev;
    }

    @AfterTest
    public void tearDown() {
        if (androidDriver != null){
            androidDriver.quit();
        }
    }
}
