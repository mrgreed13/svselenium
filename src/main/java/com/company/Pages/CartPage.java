package com.company.Pages;

import com.company.entity.Cart;
import com.company.entity.Product;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class CartPage extends BasePage {

    public void checkAndClearCart() {
        WebElement checkValue = driver.findElement(By.xpath("//img[@class='cart-icon']/following-sibling::span"));
        String temp = checkValue.getText();
        int checkEmptyCart = Integer.parseInt(temp);
        if (checkEmptyCart >= 1) {
            WebElement cartBtn = driver.findElement(By.xpath("//span[text()='Giỏ Hàng']"));
            cartBtn.click();
            Waiter.wait(1);
            WebElement selectAllProductOnCart = driver.findElement(By.xpath("//span[contains(text(),'Tất cả ')]/preceding-sibling::span"));
            selectAllProductOnCart.click();
            WebElement confirmEmptyCart = driver.findElement(By.xpath("//span[@class='productsV2__remove-all']"));
            confirmEmptyCart.click();
            Waiter.wait(1);
            WebElement confirmBtn = driver.findElement(By.xpath("//span[@class='productsV2__remove-all']/descendant::img"));
            confirmBtn.click();
            Waiter.waitForElementToBeEnable(driver.findElement(By.xpath("//div[text()='Xóa']")));
            WebElement emptyCartBtn = driver.findElement(By.xpath("//div[text()='Xóa']"));
            emptyCartBtn.click();
            Waiter.wait(1);
            WebElement returnLadingPageBtn = driver.findElement(By.xpath("//a[@class='empty__btn']"));
            Waiter.wait(1);
            returnLadingPageBtn.click();
        }
        Waiter.wait(3);
    }

    public void returnHomePage() {
        driver.get("https://tiki.vn/");
        Waiter.wait(2);
    }
    public List<WebElement> getTitleAndQuantity() {
        Waiter.wait(1);
        String cartRoot = "//div[@class = 'intended__images false']//div[@class = 'intended__content']";
        List<WebElement> listOnCart = driver.findElements(By.xpath(cartRoot + "//a[@class = 'intended__name']"));
        return listOnCart;
    }

    public List<WebElement> getPrice() {
        Waiter.wait(1);
        String cartRoot = "//div[@class = 'intended__images false']//div[@class = 'intended__content']";
        List<WebElement> priceList = driver.findElements(By.xpath(cartRoot + "//ancestor::div[@class='col-1']/following-sibling::div[@class='col-4']"));
        return priceList;
    }

    public void verifyCart() {
        Waiter.wait(1);
        String cartRoot = "//div[@class = 'intended__images false']//div[@class = 'intended__content']";
        List<WebElement> listOnCart = driver.findElements(By.xpath(cartRoot + "//a[@class = 'intended__name']"));
        List<WebElement> priceList = driver.findElements(By.xpath(cartRoot + "//ancestor::div[@class='col-1']/following-sibling::div[@class='col-4']"));

        if (listOnCart.size() > 0) {
            //verifyQuantityOfProduct
            Waiter.wait(1);
            Assert.assertEquals(listOnCart.size(), Cart.getInstance().countProduct());

            //Declare List product object & tempObj product object
            List<Product> productsOnUI = new ArrayList<>();
            Product tempObj = new Product();

            //Add product title and product price for Product on UI List
            for (int i = 0; i < listOnCart.size(); i++) {
                tempObj.setProductTitle(listOnCart.get(i).getText());

                String priceActualStr = priceList.get(i).getText();
                priceActualStr = priceActualStr.replaceAll("đ", "");
                priceActualStr = priceActualStr.replace(".", "");
                double priceActual = Double.parseDouble(priceActualStr);
                tempObj.setPrice(priceActual);

                productsOnUI.add(tempObj);
            }
            //Get List of product on Cart to compare with List of product on UI
            List<Product> productsOnCart = Cart.getProductObject();
            for (int i = 0; i < listOnCart.size(); i++) {
                //verifyTitleOfProduct
                String actualTitle = productsOnUI.get(i).getProductTitle();
                String expectedTitle = productsOnCart.get(i).getProductTitle();
                String[] actualTitleSplit = actualTitle.split(" - ");
                String[] expectedTitleSplit = expectedTitle.split(" - ");

                Assert.assertEquals(actualTitleSplit[0], expectedTitleSplit[0]);
                //Assert.assertEquals(productsOnUI.get(i).getProductTitle(), productsOnCart.get(i).getProductTitle());

                //verifyPriceOfProduct
                Assert.assertEquals(productsOnUI.get(i).getPrice(), productsOnCart.get(i).getPrice());
            }
        }
    }
}
