package com.company.Pages;

import com.company.core.BaseEntity;
import com.company.core.BrowserFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public abstract class BasePage extends BaseEntity{

    boolean isElementPresent(By xpath) {
        try {
            driver.findElement(xpath);
            return true;
        } catch (org.openqa.selenium.NoSuchElementException e) {
            return false;
        }
    }

    boolean isStringEmptyOrBlank(String str) {
        return str == null || str.trim().isEmpty();
    }

    @BeforeTest
    public void setupAndClickOnSkip() {
        BrowserFactory.initBrowser();
        driver.get(BaseEntity.getEnvironmentSetting().getProperty("url"));
        driver.manage().window().maximize();
    }

    @AfterTest
    public void tearDown() {
        driver.quit();
    }



}