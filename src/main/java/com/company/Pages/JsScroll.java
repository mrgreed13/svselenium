package com.company.Pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

public class JsScroll extends BasePage{
    public static JavascriptExecutor scroll = (JavascriptExecutor) driver;

    public static void srollDown (int pixel) {
        String height = "window.scrollBy(0," +  pixel + ")";
        scroll.executeScript(height);
    }

    public static void scrollDownToSpecificElement(WebElement element) {
        scroll.executeScript("arguments[0].scrollIntoView(true);", element);
    }
}
