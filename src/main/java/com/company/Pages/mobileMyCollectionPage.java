package com.company.Pages;

import com.company.core.mobileBaseEntity;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class mobileMyCollectionPage extends mobileBasePage{
    private final String myCollectionTitle = "//android.widget.TextView[@content-desc=\"Bộ sưu tập của tôi/Text\"]";
    private final String myCollectionBackIcon = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.ImageView";
    private final String searchPlaceHolder = "//android.widget.TextView[@content-desc=\"Tìm kiếm bộ sưu tập của bạn/Text\"]";
    private final String tatCaBtn = "(//android.widget.TextView[@content-desc=\"Tất cả/Text\"])[1]";
    private final String cuaToiBtn = "//android.widget.TextView[@content-desc=\"Của tôi/Text\"]";
    private final String topTrendingBtn = "//android.widget.TextView[@content-desc=\"Top Trending/Text\"]";
    private final String yeuThichCover = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ImageView";
    private final String yeuThichTitle = "//android.widget.TextView[@content-desc=\"Yêu thích/Text\"]";
    private final String quantityOfYeuThich = "//android.widget.TextView[@content-desc=\"Yêu thích/Text\"]";
    private final String taoBSTBtn = "//android.widget.TextView[@content-desc=\"Tạo BST/Text\"]";

    private final String newCollectionTemp = "//*[starts-with(@content-desc, 'viewCollectionDetail') and @index = 2]//descendant::android.widget.TextView[1]";

    public String getMyCollectionTitle() {
        mobileWaiter.waitForElementToBeDisplay(myCollectionTitle);
        MobileElement myCollectionHeading = mobileBaseEntity.androidDriver.findElement(By.xpath(myCollectionTitle));
        return myCollectionHeading.getText();
    }

    public String getSearchPlaceHolder() {
        mobileWaiter.waitForElementToBeDisplay(searchPlaceHolder);
        MobileElement searchPlaceHolderText = mobileBaseEntity.androidDriver.findElement(By.xpath(searchPlaceHolder));
        return searchPlaceHolderText.getText();
    }

    public String getDefaultFilterBtn() {
        mobileWaiter.waitForElementToBeDisplay(tatCaBtn);
        MobileElement defaultBtnText = mobileBaseEntity.androidDriver.findElement(By.xpath(tatCaBtn));
        return defaultBtnText.getText();
    }

    public String getMyOwnBtn() {
        mobileWaiter.waitForElementToBeDisplay(cuaToiBtn);
        MobileElement myOwnBtnText = mobileBaseEntity.androidDriver.findElement(By.xpath(cuaToiBtn));
        return myOwnBtnText.getText();
    }

    public String getTopTrendingBtn() {
        mobileWaiter.waitForElementToBeDisplay(topTrendingBtn);
        MobileElement topTrendingBtnText = mobileBaseEntity.androidDriver.findElement(By.xpath(topTrendingBtn));
        return topTrendingBtnText.getText();
    }

    public String getDefaultCollection() {
        mobileWaiter.waitForElementToBeDisplay(yeuThichTitle);
        MobileElement myFavoriteCollection = mobileBaseEntity.androidDriver.findElement(By.xpath(yeuThichTitle));
        return myFavoriteCollection.getText();
    }

    public void tapOnCreateANewCollectionBtn() {
        mobileWaiter.waitForElementToBeDisplay(taoBSTBtn);
        MobileElement createANewCollectionBtn = mobileBaseEntity.androidDriver.findElement(By.xpath(taoBSTBtn));
        createANewCollectionBtn.click();
    }

    public String getNewCollectionName() {
        mobileWaiter.waitForElementToBeDisplay(newCollectionTemp);
        MobileElement newCollectionName = mobileBaseEntity.androidDriver.findElement(By.xpath(newCollectionTemp));
        return newCollectionName.getText();
    }
}
