package com.company.Pages;

import models.baseData;
import models.collectionData;
import models.getOAItemAtHome;
import models.getUserCollection;
import org.json.simple.JSONObject;

import java.util.ArrayList;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.notNullValue;

public class mobileAPI extends mobileBasePage{
    private static final String mobileEndpoint = "http://api-dev.mservice.io/routerx-internal-app-api-merchant/api";

    public void verifyGetUserCollection() {
        collectionData data = new collectionData(
                10,
                0,
                "createdDate,desc"
        );
        getUserCollection request = new getUserCollection(
                "get-customer-collections",
                "vi",
                "0938765013",
                data
        );

        given().body(request).
                when().
                post("/api").
                then().
                log().
                headers().
                assertThat().
                statusCode(200).
                header("content-type", equalTo("application/json")).
                body("status", equalTo(0)).
                body("message", equalTo("Thành công")).
                body("data.content.size()", greaterThan(0)).
                body("data.content.id", everyItem(notNullValue())).
                body("data.content.name", everyItem(notNullValue())).
                body("data.content.bannerUrl", everyItem(notNullValue())).
                body("data.content.banner", everyItem(notNullValue()));

    }

    public String getTheNewestCollection(){
        collectionData data = new collectionData(
                10,
                0,
                "createdDate,desc"
        );
        getUserCollection request = new getUserCollection(
                "get-customer-collections",
                "vi",
                "0938765013",
                data
        );

        ArrayList<String> collectionName = given().body(request).
                when().post("/api").
                then().extract().path("data.content.name");
        return collectionName.get(1);
    }

    public void verifyGetOAAtHomeAPI(){
        baseData data = new baseData(
                10,
                0,
                10.7725168,
                106.6980208
        );
        getOAItemAtHome request = new getOAItemAtHome(
                "tdmm-get-home-recommend-oas",
                "vi",
                "0938765013",
                data
        );

        given().body(request).
        when().
                post("/api").
        then().
                log().
                    headers().
                    assertThat().
                    statusCode(200).
                    header("content-type", equalTo("application/json")).
                    body("status", equalTo(0)).
                    body("message", equalTo("Thành công")).
                    body("data.content.size()", greaterThan(0)).
                    body("data.content.id", everyItem(notNullValue())).
                    body("data.content.name", everyItem(notNullValue()));
                    //body("data.content.id[0]", equalTo(3930)).
                    //body("data.content.name[0]", equalTo("Lẩu và Nướng"));
    }
    public String getTheFisrtOAName(){
        baseData data = new baseData(
                10,
                0,
                10.7725168,
                106.6980208
        );
        getOAItemAtHome request = new getOAItemAtHome(
                "tdmm-get-home-recommend-oas",
                "vi",
                "0938765013",
                data
        );

/*        var resposne = given().body(request).
                when().post(mobileEndpoint).
                then();
        resposne.log().body();*/

        ArrayList<String> oaName = given().body(request).
                when().post("/api").
                then().extract().path("data.content.name");
        return oaName.get(0);
    }

}
