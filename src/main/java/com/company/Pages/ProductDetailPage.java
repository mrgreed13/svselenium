package com.company.Pages;

import com.company.entity.Cart;
import com.company.entity.Product;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

public class ProductDetailPage extends BasePage {


    public void purchaseProductAndClickOnCart() {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement buyIt = driver.findElement(By.xpath("//button[text()='Chọn mua']"));
        buyIt.click();
        Waiter.wait(1);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement checkValue = driver.findElement(By.xpath("//img[@class='cart-icon']/following-sibling::span"));
        String temp = checkValue.getText();
        int checkEmptyCart = Integer.parseInt(temp);
        if (checkEmptyCart > 0) {
            WebElement cartBtn = driver.findElement(By.xpath("//span[text()='Giỏ Hàng']"));
            Product product = new Product();

            //HavingCapacityAndColor
            if (isElementPresent(By.xpath("//div[@data-view-id='pdp_main_select_configuration_item' and contains(@class,'active')]/child::span")) && isElementPresent(By.xpath("//button[@data-view-id='pdp_main_select_configuration_item' and contains(@class,'active')]"))) {
                WebElement productTitle = driver.findElement(By.xpath("//h1[@class='title']"));
                WebElement capacity = driver.findElement(By.xpath("//button[@data-view-id='pdp_main_select_configuration_item' and contains(@class,'active')]"));
                WebElement color = driver.findElement(By.xpath("//div[@data-view-id='pdp_main_select_configuration_item' and contains(@class,'active')]/child::span"));
                String capacityText = capacity.getText();
                String colorText = color.getText();
                //String fullTitle = productTitle.getText() + " - " + capacityText + " - " + colorText;
                String fullTitle = String.format("%s - %s - %s", productTitle.getText(), capacityText, colorText);
                product.setProductTitle(fullTitle);
            }
            //OnlyColor
            else if (isElementPresent(By.xpath("//div[@data-view-id='pdp_main_select_configuration_item' and contains(@class,'active')]/child::span"))) {
                WebElement productTitle = driver.findElement(By.xpath("//h1[@class='title']"));
                WebElement color = driver.findElement(By.xpath("//div[@data-view-id='pdp_main_select_configuration_item' and contains(@class,'active')]/child::span"));
                String colorText = color.getText();
                //String fullTitle = productTitle.getText() + " - " + colorText;
                String fullTitle = String.format("%s - %s", productTitle.getText(), colorText);
                product.setProductTitle(fullTitle);
            }
            //OnlyCapacity
            else if (isElementPresent(By.xpath("//button[@data-view-id='pdp_main_select_configuration_item' and contains(@class,'active')]"))) {
                WebElement productTitle = driver.findElement(By.xpath("//h1[@class='title']"));
                WebElement capacity = driver.findElement(By.xpath("//button[@data-view-id='pdp_main_select_configuration_item' and contains(@class,'active')]"));
                String capacityText = capacity.getText();
                //String fullTitle = productTitle.getText() + " - " + capacityText;
                String fullTitle = String.format("%s - %s", productTitle.getText(), capacityText);
                product.setProductTitle(fullTitle);
            } else {
                WebElement productTitle = driver.findElement(By.xpath("//h1[@class='title']"));
                String fullTitle = productTitle.getText();
                product.setProductTitle(fullTitle);
            }

            if (isElementPresent(By.xpath("//div[@class='price-and-icon ']"))) {
                WebElement productPrice = driver.findElement(By.xpath("//span[@class='product-price__current-price']"));
                String priceAsString = productPrice.getText();

                priceAsString = priceAsString.replaceAll(" ₫", "");
                priceAsString = priceAsString.replace(".", "");
                double priceAsDouble = Double.parseDouble(priceAsString);
                product.setPrice(priceAsDouble);

                Cart.getInstance().addProductToCart(product);

                cartBtn.click();
            } else if (isElementPresent(By.xpath("//div[@class='price-and-icon no-background']"))) {
                WebElement productPrice = driver.findElement(By.xpath("//div[@class='flash-sale-price']/child::span"));
                String priceAsString = productPrice.getText();

                priceAsString = priceAsString.replaceAll(" ₫", "");
                priceAsString = priceAsString.replace(".", "");

                double priceAsDouble = Double.parseDouble(priceAsString);
                product.setPrice(priceAsDouble);

                Cart.getInstance().addProductToCart(product);

                cartBtn.click();
            }
        } else {
            System.out.println("This product did not add to the Cart yet."
                    + "\n" + driver.getCurrentUrl()
                    + "\n" + "Please re-check this product.");
            driver.quit();
        }
    }


}
