package com.company.Pages;

import org.testng.Assert;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.Date;
import java.util.Locale;
import java.util.SimpleTimeZone;

public class DateTimeUtil extends BasePage {
    public static DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/u").withResolverStyle(ResolverStyle.STRICT);

    public static String returnTomorrow(int x, DateTimeFormatter formatter) {
        LocalDate tomorrowDate = LocalDate.now(ZoneId.of("Asia/Ho_Chi_Minh")).plusDays(x);
        return tomorrowDate.format(formatter);
    }
    // String formatter, convert nó sang dateFormatter
    public static boolean isValid(String dateStr, DateTimeFormatter formatter) {
        try {
            LocalDate.parse(dateStr, formatter);
        } catch (DateTimeParseException e) {
            return false;
        }
        return true;
    }

    public static void main(String[] agrs) {
        //System.out.println(isValid("31/12/2021", dateFormatter));
        String tomorrow = returnTomorrow(2, dateFormatter);
        System.out.println(tomorrow);
    }
}
