package com.company.Pages;

import org.apache.commons.validator.GenericValidator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.Calendar;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class PickUpDateOnCalendar extends BasePage {
    public void pickUpDate(String dateStr) {
        WebElement exitCalendar = driver.findElement(By.xpath("//h1[@class='page-header']"));
        if (GenericValidator.isDate(dateStr, "MM/dd/yyyy", true)) {
            String[] splitStr = dateStr.split("/");
            List<String> listStr = new ArrayList<>();
            for (String tempStr : splitStr) {
                int tempInt = Integer.parseInt(tempStr);
                String completedStr = String.valueOf(tempInt);
                listStr.add(completedStr);
            }

            WebElement dateField = driver.findElement(By.xpath("//input[@class='form-control docs-date']"));
            dateField.click();
            Waiter.wait(1);

            int currentYear = Calendar.getInstance().get(Calendar.YEAR);
            String strYear = listStr.get(2);
            int tempYear = Integer.parseInt(strYear);
            int checkYear = tempYear - currentYear;

            String strMonth = listStr.get(0);
            int tempMonth = Integer.parseInt(strMonth);
            String strDay = listStr.get(1);
            int tempDay = Integer.parseInt(strDay);

            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            WebElement monthClick = driver.findElement(By.xpath("//li[@data-view='month current']"));

            if (checkYear > 0) {
                monthClick.click();
                driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
                WebElement nextYearBtn = driver.findElement(By.xpath("//li[@data-view='year next']"));

                for (int i = 0; i < checkYear; i++) {
                    nextYearBtn.click();
                }

                driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
                List<WebElement> monthOfYear = driver.findElements(By.xpath("//ul[@data-view='months']/li"));
                WebElement selectMonth = monthOfYear.get(tempMonth - 1);
                selectMonth.click();

                driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
                List<WebElement> dayOfMonth = driver.findElements(By.xpath("//li[contains(@data-view,'day') and not(contains(@class,'muted'))]"));
                WebElement selectDay = dayOfMonth.get(tempDay - 1);
                selectDay.click();

                exitCalendar.click();

                String getDateValue = driver.findElement(By.xpath("//input[@class='form-control docs-date']")).getAttribute("value");
                Assert.assertEquals(getDateValue, dateStr);

            } else if (checkYear == 0) {
                monthClick.click();
                driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
                List<WebElement> monthOfYear = driver.findElements(By.xpath("//ul[@data-view='months']/li"));
                WebElement selectMonth = monthOfYear.get(tempMonth - 1);
                selectMonth.click();

                driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
                List<WebElement> dayOfMonth = driver.findElements(By.xpath("//li[contains(@data-view,'day') and not(contains(@class,'muted'))]"));
                WebElement selectDay = dayOfMonth.get(tempDay - 1);
                selectDay.click();

                exitCalendar.click();

                String getDateValue = driver.findElement(By.xpath("//input[@class='form-control docs-date']")).getAttribute("value");
                Assert.assertEquals(getDateValue, dateStr);

            } else {
                monthClick.click();
                driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
                WebElement previousYearBtn = driver.findElement(By.xpath("//li[@data-view='year prev']"));

                for (int i = 0; i > checkYear; i--) {
                    previousYearBtn.click();
                }

                driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
                List<WebElement> monthOfYear = driver.findElements(By.xpath("//ul[@data-view='months']/li"));
                WebElement selectMonth = monthOfYear.get(tempMonth - 1);
                selectMonth.click();

                driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
                List<WebElement> dayOfMonth = driver.findElements(By.xpath("//li[contains(@data-view,'day') and not(contains(@class,'muted'))]"));
                WebElement selectDay = dayOfMonth.get(tempDay - 1);
                selectDay.click();

                exitCalendar.click();

                String getDateValue = driver.findElement(By.xpath("//input[@class='form-control docs-date']")).getAttribute("value");
                Assert.assertEquals(getDateValue, dateStr);

            }
        } else {
            System.out.println("This is invalid date: " + dateStr
                    + "\n" + "Please input the valid date with valid format date. Ex: dd/mm/yyyy");
        }
    }
}
