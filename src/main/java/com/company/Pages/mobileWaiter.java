package com.company.Pages;
import com.company.core.mobileBaseEntity;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class mobileWaiter extends mobileBasePage {
    public static WebDriverWait mobileWait = new WebDriverWait(mobileBaseEntity.androidDriver, 60);

    public static void waitForElementToBeDisplay(String xPath) {
        mobileWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPath)));
    }

    public static void wait(int second) {
        try {
            Thread.sleep(second * 1000);
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }
}
