package com.company.Pages;

import com.company.core.mobileBaseEntity;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class mobileAccountPage extends mobileBasePage{
    //Account Screen
    private final String accountTitle = "//*[starts-with(@text, 'Trang cá nhâ')]";

    private final String accessAccountScreenCTA = "//android.widget.TextView[@content-desc=\"Bộ sưu tập của tôi/Text\"]";
    private final String userNameAtAccountScreen = "(//android.view.ViewGroup[@content-desc=\"Block\"])[5]/child::android.widget.TextView";
    private final String userAvatarAtAccountScreen = "(//android.view.ViewGroup[@content-desc=\"Block\"])[3]/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.ImageView";

    public String getAccountTitlePage() {
        mobileWaiter.waitForElementToBeDisplay(accountTitle);
        MobileElement accountTitlePage = mobileBaseEntity.androidDriver.findElement(By.xpath(accountTitle));
        return accountTitlePage.getText();
    }



    public String getUserName() {
        mobileWaiter.waitForElementToBeDisplay(userNameAtAccountScreen);
        MobileElement userName = mobileBaseEntity.androidDriver.findElement(By.xpath(userNameAtAccountScreen));
        return userName.getText();
    }

    public String getMyCollectionCTA() {
        mobileWaiter.waitForElementToBeDisplay(accessAccountScreenCTA);
        MobileElement bstCuaToiBtn = mobileBaseEntity.androidDriver.findElement(By.xpath(accessAccountScreenCTA));
        return bstCuaToiBtn.getText();
    }

    public void accessMyCollectionPage() {
        mobileWaiter.waitForElementToBeDisplay(accessAccountScreenCTA);
        MobileElement bstCuaToiBtn = mobileBaseEntity.androidDriver.findElement(By.xpath(accessAccountScreenCTA));
        bstCuaToiBtn.click();
    }
}
