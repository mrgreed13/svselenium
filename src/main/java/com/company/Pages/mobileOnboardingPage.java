package com.company.Pages;

import com.company.core.mobileBaseEntity;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class mobileOnboardingPage extends mobileBasePage{
    private final String obKhamPhaBtnText = "//android.view.ViewGroup[@content-desc=\"Khám phá ngay/Button\"]";
    public void tapOnKPBtn() {
        mobileWaiter.waitForElementToBeDisplay(obKhamPhaBtnText);
        MobileElement obKhamPhaBtn = mobileBaseEntity.androidDriver.findElement(By.xpath(obKhamPhaBtnText));
        obKhamPhaBtn.click();
    }
}
