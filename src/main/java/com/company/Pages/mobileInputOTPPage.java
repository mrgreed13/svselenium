package com.company.Pages;

import com.company.core.mobileBaseEntity;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

public class mobileInputOTPPage extends mobileBasePage{
    private final String otpFld = "//android.widget.EditText[@content-desc=\"OTPTextInput\"]";

/*
    @FindBy(xpath = otpFld)
    MobileElement otpField;
*/

    public void inputOTP(String otp) {
        mobileGestures.hideKeyBoard();
        mobileWaiter.waitForElementToBeDisplay(otpFld);
        MobileElement otpField = mobileBaseEntity.androidDriver.findElement(By.xpath(otpFld));
        otpField.sendKeys(otp);
    }
}