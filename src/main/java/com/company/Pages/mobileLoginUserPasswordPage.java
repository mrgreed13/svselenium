package com.company.Pages;

import com.company.core.mobileBaseEntity;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class mobileLoginUserPasswordPage extends mobileBasePage{
    private final String loginUserPasswordField = "//android.widget.EditText[@content-desc=\"Nhập mật khẩu/TextInput\"]";

    public void inputUserPassword(String password) {
        mobileWaiter.waitForElementToBeDisplay(loginUserPasswordField);
        MobileElement userPasswordField = mobileBaseEntity.androidDriver.findElement(By.xpath(loginUserPasswordField));
        userPasswordField.sendKeys(password);
    }
}
