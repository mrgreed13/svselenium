package com.company.Pages;

import com.company.core.mobileBaseEntity;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class mobileLoginUserPhoneNumberPage extends mobileBasePage{
    private final String loginPhoneNumberField = "//android.widget.EditText[@content-desc=\"Số điện thoại/TextInput\"]";
    private final String nextBtn = "//android.view.ViewGroup[@content-desc=\"Tiếp tục/Button\"]";

    private final String callMe = "//android.view.ViewGroup[@content-desc=\"Gọi cho tôi/Button\"]";

    public void inputUserMobileNumber(String phoneNumber) {
        mobileWaiter.waitForElementToBeDisplay(loginPhoneNumberField);
        MobileElement userPhoneNumberField = androidDriver.findElement(By.xpath(loginPhoneNumberField));
        userPhoneNumberField.sendKeys(phoneNumber);

        mobileWaiter.waitForElementToBeDisplay(nextBtn);
        MobileElement nextButton = mobileBaseEntity.androidDriver.findElement(By.xpath(nextBtn));
        nextButton.click();

        mobileWaiter.waitForElementToBeDisplay(callMe);
        MobileElement callMeBtn = androidDriver.findElement(By.xpath(callMe));
        callMeBtn.click();
    }
}
