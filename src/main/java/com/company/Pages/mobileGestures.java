package com.company.Pages;

import com.company.core.mobileBaseEntity;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import org.openqa.selenium.interactions.Interaction;
import org.openqa.selenium.interactions.PointerInput;

import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static io.appium.java_client.touch.TapOptions.tapOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;
import static io.appium.java_client.touch.offset.PointOption.point;

public class mobileGestures extends mobileBasePage{
    public static PointerInput finger = new PointerInput(PointerInput.Kind.TOUCH,"finger");
    Interaction moveToStart = finger.createPointerMove(Duration.ZERO, PointerInput.Origin.viewport(), 520, 1530);
    Interaction pressDown = finger.createPointerDown(PointerInput.MouseButton.LEFT.asArg());
    Interaction moveToEnd = finger.createPointerMove(Duration.ofMillis(1000), PointerInput.Origin.viewport(), 520, 490);
    Interaction pressUp = finger.createPointerUp(PointerInput.MouseButton.LEFT.asArg());

    public static TouchAction action = new TouchAction(mobileBaseEntity.androidDriver);

    //Tap to an element for 250 milliseconds
    public static void tapByElement (MobileElement androidElement) {
        action
                .tap(tapOptions().withElement(element(androidElement)))
                .perform();
    }
    //Tap by coordinates
    public static void tapByCoordinates (int x,  int y) {
        action
                .tap(point(x,y))
                .perform();
    }
    public static void hideKeyBoard() {
        mobileBaseEntity.androidDriver.hideKeyboard();
    }
}
