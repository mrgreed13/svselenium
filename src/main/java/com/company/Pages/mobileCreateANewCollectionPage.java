package com.company.Pages;

import com.company.core.mobileBaseEntity;
import io.appium.java_client.MobileElement;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.offset.ElementOption;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.PointerInput;

public class mobileCreateANewCollectionPage extends mobileBasePage{
    private final String pageTitle = "//android.widget.ScrollView[@content-desc=\"viewCollectionCreate\"]/following-sibling::android.view.ViewGroup[1]/android.widget.TextView";

    private final String inputTitleFld = "//android.widget.TextView[@content-desc=\"Tên bộ sưu tập (0/48) /Text\"]";
    private final String countLetterNumber = "//android.widget.TextView[@content-desc=\"(0/48)/Text\"]";
    private final String inputPlaceHolderText = "//android.widget.EditText[@content-desc=\"Tên bộ sưu tập (0/48) /TextInput\"]";

    private final String createBtn = "//android.view.ViewGroup[@content-desc=\"Tạo/Button\"]";


    public void createANewCollection(String newCollectionName) {


        mobileWaiter.waitForElementToBeDisplay(inputPlaceHolderText);
        MobileElement inputField = mobileBaseEntity.androidDriver.findElement(By.xpath(inputPlaceHolderText));
        inputField.sendKeys(newCollectionName);

//        mobileWaiter.waitForElementToBeDisplay(inputTitleFld);
//        MobileElement inputTitle = mobileBaseEntity.androidDriver.findElement(By.xpath(inputTitleFld));
        mobileGestures.tapByCoordinates(537, 420);

        mobileWaiter.waitForElementToBeDisplay(createBtn);
        MobileElement taoBtn = mobileBaseEntity.androidDriver.findElement(By.xpath(createBtn));
        taoBtn.click();
    }
    public String getPageTitle() {
        mobileGestures.hideKeyBoard();
        mobileWaiter.waitForElementToBeDisplay(pageTitle);
        MobileElement pageHeading = mobileBaseEntity.androidDriver.findElement(By.xpath(pageTitle));
        return pageHeading.getText();
    }

    public String getInputTitleText() {
        mobileWaiter.waitForElementToBeDisplay(inputTitleFld);
        MobileElement inputTitleText = mobileBaseEntity.androidDriver.findElement(By.xpath(inputTitleFld));
        return inputTitleText.getText();
    }

    public String getCountLetterNumber() {
        mobileWaiter.waitForElementToBeDisplay(countLetterNumber);
        MobileElement countLetterNo = mobileBaseEntity.androidDriver.findElement(By.xpath(countLetterNumber));
        return countLetterNo.getText();
    }

    public String getInputPlaceHolderText() {
        mobileWaiter.waitForElementToBeDisplay(inputPlaceHolderText);
        MobileElement inputPlaceHolder = mobileBaseEntity.androidDriver.findElement(By.xpath(inputPlaceHolderText));
        return inputPlaceHolder.getText();
    }

    public String getCreateBtn() {
        mobileWaiter.waitForElementToBeDisplay(createBtn);
        MobileElement taoBtn = mobileBaseEntity.androidDriver.findElement(By.xpath(createBtn));
        return taoBtn.getText();
    }

}
