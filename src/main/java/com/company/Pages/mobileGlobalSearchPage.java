package com.company.Pages;

import com.company.core.mobileBaseEntity;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class mobileGlobalSearchPage extends mobileBasePage{
    private final String searchFld1 = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.EditText";
    private final String searchFld = "//*[starts-with(@text, 'Tìm ')]";
    //private final String searchFld = "//*[@text=\"Tìm \")]";
    private final String tdmmIc = "//android.view.ViewGroup[@content-desc=\"oa_local_discovery\"]";
    private final String tdmmName = "//android.view.ViewGroup[@content-desc=\"oa_local_discovery\"]/child::android.widget.TextView[1]";
    ////android.view.ViewGroup[@content-desc="oa_local_discovery"]
    //oa_local_discovery
    public void searchMiniApp(String miniApp) {
        mobileWaiter.waitForElementToBeDisplay(searchFld);
        MobileElement searchField = mobileBaseEntity.androidDriver.findElement(By.xpath(searchFld));
        searchField.sendKeys(miniApp);
        mobileWaiter.wait(5);
    }

    public String getMiniAppName() {
        mobileWaiter.waitForElementToBeDisplay(tdmmName);
        MobileElement tdmmMiniAppName = mobileBaseEntity.androidDriver.findElement(By.xpath(tdmmName));
        return tdmmMiniAppName.getText();
    }

    public void tapOnResult() {
        mobileWaiter.waitForElementToBeDisplay(tdmmIc);
        MobileElement tdmmIcon = mobileBaseEntity.androidDriver.findElement(By.xpath(tdmmIc));
        tdmmIcon.click();
    }
}
