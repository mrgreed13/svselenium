package com.company.Pages;

import com.company.Sex;
import org.apache.commons.validator.GenericValidator;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static com.company.Pages.DateTimeUtil.dateFormatter;

public class ProfilePage extends BasePage {

/*    private final WebElement fullNameSection = driver.findElement(By.xpath("//input[@name='fullName']"));
    private final WebElement updateBtn = driver.findElement(By.xpath("//button[text()='Cập nhật']"));
    private final WebElement maleRatio = driver.findElement(By.xpath("//span[text()='Nam']/preceding-sibling::span"));
    private final WebElement maleRatioCheckedOrnot = driver.findElement(By.xpath("//span[text()='Nam']/preceding-sibling::input"));
    private final WebElement femaleRatio = driver.findElement(By.xpath("//span[text()='Nữ']/preceding-sibling::span"));
    private final WebElement femaleRatioCheckedOrnot = driver.findElement(By.xpath("//span[text()='Nữ']/preceding-sibling::input"));*/

    private final String fullNameSectionXpath = "//input[@name='fullName']";
    private final String updateBtnXpath = "//button[text()='Lưu thay đổi']";
    private final String maleRatioXpath = "//span[text()='Nam']/preceding-sibling::span";
    private final String maleRatioCheckedOrnotXpath = "//span[text()='Nam']/preceding-sibling::input";
    private final String femaleRatioXpath = "//span[text()='Nữ']/preceding-sibling::span";
    private final String femaleRatioCheckedOrnotXpath = "//span[text()='Nữ']/preceding-sibling::input";
    private final String otherRatioXpath = "//span[text()='Khác']/preceding-sibling::span";
    private final String otherRatioCheckedOrnotXpath = "//span[text()='Khác']/preceding-sibling::input";
    private int checkFullName;
    private List<String> listDOB = new ArrayList<>();

    public void inputNewFullName(String fullName) {
        WebElement fullNameSection = driver.findElement(By.xpath(fullNameSectionXpath));
        //fullNameSection.clear();
        fullNameSection.sendKeys(Keys.chord(Keys.CONTROL,"a", Keys.DELETE));
        Waiter.wait(2);
        fullNameSection.sendKeys(fullName);
        WebElement updateBtn = driver.findElement(By.xpath(updateBtnXpath));
        updateBtn.click();
        Waiter.wait(2);
        if (isElementPresent(By.xpath("//div[text()='Vui lòng nhập họ tên']"))) {
            checkFullName = 1;
        } else if (isElementPresent(By.xpath("//div[text()='Tên phải chứa ít nhất 2 từ']"))) {
            checkFullName = 2;
        } else if (isElementPresent(By.xpath("//div[text()='Độ dài tối đa cho \"họ tên\" là 64 kí tự']"))) {
            checkFullName = 3;
        } else {
            checkFullName = 4;
        }
    }

    public String getReminderMessageAfterUpdate() {
        String reminderMessage = null;
        switch (checkFullName) {
            case 1:
                WebElement blankFullName = driver.findElement(By.xpath("//div[text()='Vui lòng nhập họ tên']"));
                reminderMessage = blankFullName.getText();
                break;
            case 2:
                WebElement invalidFullName = driver.findElement(By.xpath("//div[text()='Tên phải chứa ít nhất 2 từ']"));
                reminderMessage = invalidFullName.getText();
                break;
            case 3:
                WebElement invalidFullNameWithSixtyFiveLetter = driver.findElement(By.xpath("//div[text()='Độ dài tối đa cho \"họ tên\" là 64 kí tự']"));
                reminderMessage = invalidFullNameWithSixtyFiveLetter.getText();
                break;
        }
        return reminderMessage;
    }

    public String getNotificationMessage(String successOrFail) {
        if (successOrFail.equals("successful")) {
            //Waiter.waitForElementToBeDisplay(driver.findElement(By.xpath("//div[@type='success']/span")));
            //WebElement successfulMessage = driver.findElement(By.xpath("//div[@type='success']/span"));
            Waiter.waitForElementToBeDisplay(driver.findElement(By.xpath("//p[text()='Cập nhật thông tin thành công']")));
            WebElement successfulMessage = driver.findElement(By.xpath("//p[text()='Cập nhật thông tin thành công']"));
            return successfulMessage.getText();
        } else {
            Waiter.waitForElementToBeDisplay(driver.findElement(By.xpath("//div[@type='danger']/span")));
            WebElement failedMessage = driver.findElement(By.xpath("//div[@type='danger']/span"));
            return failedMessage.getText();
        }
    }

    public String getFullName(boolean refreshOrNot) {
        String fullName;
        if (!refreshOrNot) {
            WebElement fullNameSection = driver.findElement(By.xpath(fullNameSectionXpath));
            Waiter.waitForElementToBeDisplay(fullNameSection);
            fullName = fullNameSection.getAttribute("value");
        } else {
            driver.navigate().refresh();
            Waiter.wait(2);
            WebElement fullNameSection = driver.findElement(By.xpath(fullNameSectionXpath));
            Waiter.waitForElementToBeDisplay(fullNameSection);
            fullName = fullNameSection.getAttribute("value");
        }
        return fullName;
    }

    public void selectSexRadio(Sex sex) {
        WebElement updateBtn = driver.findElement(By.xpath(updateBtnXpath));
        WebElement maleRatio = driver.findElement(By.xpath(maleRatioXpath));
        WebElement femaleRatio = driver.findElement(By.xpath(femaleRatioXpath));
        WebElement otherRatio = driver.findElement(By.xpath(otherRatioXpath));
        switch (sex) {
            case Nam:
                maleRatio.click();
                updateBtn.click();
                Waiter.wait(1);
                break;
            case Nữ:
                femaleRatio.click();
                updateBtn.click();
                Waiter.wait(1);
                break;
            case Khác:
                otherRatio.click();
                updateBtn.click();
                Waiter.wait(1);
                break;
        }
    }

    public boolean verifyChecked(Sex sex, String refreshOrNot) {
        boolean checkPoint = false;
        switch (refreshOrNot) {
            case "no":
                WebElement maleRatioCheckedOrNot = driver.findElement(By.xpath(maleRatioCheckedOrnotXpath));
                WebElement femaleRatioCheckedOrNot = driver.findElement(By.xpath(femaleRatioCheckedOrnotXpath));
                WebElement otherRatioCheckedOrNot = driver.findElement(By.xpath(otherRatioCheckedOrnotXpath));
                switch (sex) {
                    case Nam:
                        if (maleRatioCheckedOrNot.isSelected() && !femaleRatioCheckedOrNot.isSelected() && !otherRatioCheckedOrNot.isSelected()) {
                            checkPoint = true;
                        }
                        break;
                    case Nữ:
                        if (femaleRatioCheckedOrNot.isSelected() && !maleRatioCheckedOrNot.isSelected() && !otherRatioCheckedOrNot.isSelected()) {
                            checkPoint = true;
                        }
                    case Khác:
                        if (otherRatioCheckedOrNot.isSelected() && !maleRatioCheckedOrNot.isSelected() && !femaleRatioCheckedOrNot.isSelected()) {
                            checkPoint = true;
                        }
                }
                break;
            case "yes":
                driver.navigate().refresh();
                Waiter.wait(2);
                maleRatioCheckedOrNot = driver.findElement(By.xpath(maleRatioCheckedOrnotXpath));
                femaleRatioCheckedOrNot = driver.findElement(By.xpath(femaleRatioCheckedOrnotXpath));
                otherRatioCheckedOrNot = driver.findElement(By.xpath(otherRatioCheckedOrnotXpath));
                switch (sex) {
                    case Nam:
                        if (maleRatioCheckedOrNot.isSelected() && !femaleRatioCheckedOrNot.isSelected()) {
                            checkPoint = true;
                        }
                        break;
                    case Nữ:
                        if (femaleRatioCheckedOrNot.isSelected() && !maleRatioCheckedOrNot.isSelected() && !otherRatioCheckedOrNot.isSelected()) {
                            checkPoint = true;
                        }
                    case Khác:
                        if (otherRatioCheckedOrNot.isSelected() && !maleRatioCheckedOrNot.isSelected() && !femaleRatioCheckedOrNot.isSelected()) {
                        checkPoint = true;
                    }
                }
                break;
        }
        return checkPoint;
    }

    public void checkAndInputNewDOB(String dob) {
        if (DateTimeUtil.isValid(dob, dateFormatter)) {
            LocalDate minDate = LocalDate.of(1956, 1, 1).minusDays(1);
            LocalDate maxDate = LocalDate.of(2021, 12, 31).plusDays(1);
            LocalDate strDate = LocalDate.parse(dob, dateFormatter);
            //Check the valid year [1956, 2021]
            if (strDate.compareTo(minDate) > 0 && strDate.compareTo(maxDate) < 0) {
                String[] splitDOB = dob.split("/");
                for (String tempStr : splitDOB) {
                    int tempInt = Integer.parseInt(tempStr);
                    String completedStr = String.valueOf(tempInt);
                    listDOB.add(completedStr);
                }
                Select yearOfDOB = new Select(driver.findElement(By.xpath("//select[@name = 'year']")));
                yearOfDOB.selectByVisibleText(listDOB.get(2));
                Select monthOfDOB = new Select(driver.findElement(By.xpath("//select[@name = 'month']")));
                monthOfDOB.selectByVisibleText(listDOB.get(1));
                Select dayOfDOB = new Select(driver.findElement(By.xpath("//select[@name = 'day']")));
                dayOfDOB.selectByVisibleText(listDOB.get(0));
                WebElement updateBtn = driver.findElement(By.xpath(updateBtnXpath));
                updateBtn.click();
            } else {
                System.out.println("The required year is less than 2022 and is greater than 1955. Please input the valid year.");
            }
        } else {
            System.out.println("This is invalid date." + "\n" + "Please input the valid date with valid format date. Ex: dd/mm/yyyy");
        }
    }

    public String getDOB(boolean refreshOrNot) {
        String currentDOB;
        String correctFormatDay;
        String correctFormatMonth;
        if (!refreshOrNot) {
            Select yearOfDOB = new Select(driver.findElement(By.xpath("//select[@name = 'year']")));
            Select monthOfDOB = new Select(driver.findElement(By.xpath("//select[@name = 'month']")));
            Select dayOfDOB = new Select(driver.findElement(By.xpath("//select[@name = 'day']")));
            int convertDay = Integer.parseInt(dayOfDOB.getFirstSelectedOption().getText());
            int convertMonth = Integer.parseInt(monthOfDOB.getFirstSelectedOption().getText());
            if (convertDay < 10) {
                correctFormatDay = String.format("0%s", dayOfDOB.getFirstSelectedOption().getText());
            } else {
                correctFormatDay = dayOfDOB.getFirstSelectedOption().getText();
            }
            if (convertMonth < 10) {
                correctFormatMonth = String.format("0%s", monthOfDOB.getFirstSelectedOption().getText());
            } else {
                correctFormatMonth = monthOfDOB.getFirstSelectedOption().getText();
            }
            currentDOB = String.format("%s/%s/%s", correctFormatDay, correctFormatMonth, yearOfDOB.getFirstSelectedOption().getText());
        } else {
            driver.navigate().refresh();
            Waiter.wait(2);
            Select yearOfDOB = new Select(driver.findElement(By.xpath("//select[@name = 'year']")));
            Select monthOfDOB = new Select(driver.findElement(By.xpath("//select[@name = 'month']")));
            Select dayOfDOB = new Select(driver.findElement(By.xpath("//select[@name = 'day']")));
            int convertDay = Integer.parseInt(dayOfDOB.getFirstSelectedOption().getText());
            int convertMonth = Integer.parseInt(monthOfDOB.getFirstSelectedOption().getText());
            if (convertDay < 10) {
                correctFormatDay = String.format("0%s", dayOfDOB.getFirstSelectedOption().getText());
            } else {
                correctFormatDay = dayOfDOB.getFirstSelectedOption().getText();
            }
            if (convertMonth < 10) {
                correctFormatMonth = String.format("0%s", monthOfDOB.getFirstSelectedOption().getText());
            } else {
                correctFormatMonth = monthOfDOB.getFirstSelectedOption().getText();
            }
            currentDOB = String.format("%s/%s/%s", correctFormatDay, correctFormatMonth, yearOfDOB.getFirstSelectedOption().getText());
        }
        return currentDOB;
    }
}
