package com.company.Pages;

import lombok.ToString;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.company.Pages.Waiter;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class LandingPageAfterLoginSuccess extends BasePage {

    //private final WebElement verifyLoginSuccessful = driver.findElement(By.xpath("//span[text()='Tài Khoản']"));
    private String verifyText = "";

    public void clickOnAccountSection() {
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        Waiter.waitForElementToBeDisplay(driver.findElement(By.xpath("//span[text()='Tài Khoản']")));
        WebElement verifyLoginSuccessful = driver.findElement(By.xpath("//span[text()='Tài Khoản']"));
        verifyLoginSuccessful.click();

        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        WebElement myAccount = driver.findElement(By.xpath("//p[text()='Tài khoản của tôi']"));
        Waiter.waitForElementToBeDisplay(myAccount);
        myAccount.click();
        Waiter.wait(2);
    }

    public String checkAccessSuccessful() {
        Waiter.wait(2);
        //Assert.assertEquals(driver.getCurrentUrl(),"https://tiki.vn/customer/account/edit?src=header_my_account");
        return driver.getCurrentUrl();
    }

    public String toString(){
        return verifyText;
    }

}
