import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        features = {"src/test/java/Features"},
        monochrome = false, //option print color in console - optional
        strict = true, //default true
        glue = {"com.company.StepDefination", "com.company.Support"},
        //tags = "@TC008 and not @skip", //skip @skip scenario
        //tags = "@ModifyUserprofileTC and not @skip",
        //tags = "@LoginTC and not @skip",
        tags = "@TC001 and not @skip",
        plugin = {
                "html:target/result", //quick report
                "pretty", //print by ..
                "json:target/test-classes/reports/result.json" //send report to the client
        }
)

public class CucumberRunner extends AbstractTestNGCucumberTests {
}