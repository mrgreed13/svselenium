@PurchaseProductTC
Feature: Tiki purchase

  @TC015
  Scenario: user can buy random product on Landing page
    Given Login and clear Cart if any then return to Landing page
    When Select random product
    Then Check product on Cart
