@LoginTC
Feature: Tiki Login

  @TC001
  Scenario: login successful with valid account
    Given Start Tiki and go to login page
    When Login with valid account
    Then Login successful with valid id

  @TC002
  Scenario: login failed with blank id
    Given Start Tiki and go to login page
    When Login with invalid account - blank id
    Then Login failed with blank id

  @TC003
  Scenario: login failed with invalid id
    Given Start Tiki and go to login page
    When Login with invalid account - invalid id
    Then Login failed with invalid id

  @TC004
  Scenario: login failed with unregistered id
    Given Start Tiki and go to login page
    When Login with invalid account - unregistered id
    Then Login failed with unregistered id

  @TC005
  Scenario: login failed with unregistered and out of verifying limit id
    Given Start Tiki and go to login page
    When Login with invalid account - unregistered and out of verifying limit id
    Then Login failed with unregistered id and out of verifying limit

  @TC006
  Scenario: login failed with blank password
    Given Start Tiki and go to login page
    When Login with invalid account - blank password
    Then Login failed with blank password

  @TC007
  Scenario: login failed with blank password
    Given Start Tiki and go to login page
    When Login with invalid account - invalid password
    Then Login failed with invalid password

