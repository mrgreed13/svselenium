@ModifyUserprofileTC
Feature: Tiki User profile
  //Nên Assert tại đây không bỏ vào method của page, method chỉ trả về giá trị
  // xong hết giải quyets StaleElementReferenceException
  //add thêm runtime language và environment
  //switch language trên trang lazada
  // cân nhắc đặt method switch language ở step nào
  // mỗi 1 môi trường là file fp

  //có cách nào khởi tạo trước các page mà khai báo sẵn webelement trước? đại khái khi chưa load page đó thì không thể locate nó trước

  @TC008
  Scenario: User can update Full name with valid value
    Given Logged in and access to Profile page
    When Update full name with valid value
    Then Verify the current full name with valid value
    And Show the successful message
    And Refresh page and double check full name with valid value

  @TC009
  Scenario: User can not update Full name with blank value
    Given Logged in and access to Profile page
    When Update full name with blank value
    Then Verify the current full name with blank value
    And Show the failed message
    And Show reminder message about blank value
    And Refresh page and double check full name with blank value

  @TC010
  Scenario: User can not update Full name with invalid value
    Given Logged in and access to Profile page
    When Update full name with invalid value
    Then Verify the current full name with invalid value
    And Show the failed message
    And Show reminder message about invalid value
    And Refresh page and double check full name with invalid value

  @TC011
  Scenario: User can not update Full name with invalid with sixty five letter value
    Given Logged in and access to Profile page
    When Update full name with invalid with sixty five letter value
    Then Verify the current full name with invalid with sixty five letter value
    And Show the failed message
    And Show reminder message about invalid with sixty five letter value
    And Refresh page and double check full name with invalid with sixty five letter value

  @TC012
  Scenario: User can select male gender
    Given Logged in and access to Profile page
    When Select male gender
    Then Update male gender successful
    And Show the successful message
    And Refresh page and double check male gender is selected

  @TC013
  Scenario: User can select female gender
    Given Logged in and access to Profile page
    When Select female gender
    Then Update female gender successful
    And Show the successful message
    And Refresh page and double check female gender is selected

  @TC014
  Scenario: User can update DOB
    Given Logged in and access to Profile page
    When Select new DOB
    Then First time check DOB update is successful
    And Show the successful message
    And Double check DOB update is successful