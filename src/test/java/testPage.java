import com.company.Pages.Waiter;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import net.bytebuddy.asm.Advice;
import org.apache.commons.validator.GenericValidator;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.*;
import java.text.ParseException;

import java.io.File;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import java.net.URL;

import static io.appium.java_client.touch.WaitOptions.waitOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;
import static java.time.Duration.ofSeconds;

public class testPage {
    public static WebDriver driver;
    //public static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    @Test
    public static void getResponseBody() {
/*        given().queryParam("CUSTOMER_ID","68195")
                .queryParam("PASSWORD","1234!")
                .queryParam("Account_No","1")
                .when()
                .get("http://demo.guru99.com/V4/sinkministatement.php")
                .then()
                .log().body();*/
        Response response = RestAssured.get("https://reqres.in/api/users?page=2");
        System.out.println("Response: " + response.asString());
        System.out.println("Status code: " + response.getStatusCode());
        System.out.println("Body: " + response.getBody().asString());
        System.out.println("Time taken: " + response.getTime());
        System.out.println("Header: " + response.getHeader("content-type"));

    }

    public static boolean isElementPresent(By xpath) {
        try {
            driver.findElement(xpath);
            return true;
        } catch (org.openqa.selenium.NoSuchElementException e) {
            return false;
        }
    }

    //Press by element

    public static boolean validateJavaDate(String strDate)
    {
        /* Check if date is 'null' */
        if (strDate.trim().equals(""))
        {
            return true;
        }
        /* Date is not 'null' */
        else
        {
            /*
             * Set preferred date format,
             * For example MM-dd-yyyy, MM.dd.yyyy,dd.MM.yyyy etc.*/
            SimpleDateFormat sdfrmt = new SimpleDateFormat("dd/MM/yyyy");
            sdfrmt.setLenient(false);
            /* Create Date object
             * parse the string into date
             */
            try
            {
                Date javaDate = sdfrmt.parse(strDate);
                System.out.println(strDate+" is valid date format");
            }
            /* Date format is invalid */
            catch (ParseException e)
            {
                System.out.println(strDate+" is Invalid Date format" + "\n" + "Please input the date format: dd/mm/yyyy");
                return false;
            }
            /* Return true if date format is valid */
            return true;
        }
    }

/*    public static void main(String[] agrs) throws Exception {
        String pathToRootDirectory = System.getProperty("user.dir");
        System.setProperty("webdriver.chrome.driver", pathToRootDirectory + File.separator + "drivers" + File.separator + "win_chromedriver.exe");
        driver = new ChromeDriver();
        WebDriverWait wait = new WebDriverWait(driver, 60);
        //driver.get("https://tiki.vn/product-p82758182.html?spid=82758230"); //capacity
        //driver.get("https://tiki.vn/bao-tay-gang-tay-xe-may-moto-di-phuot-full-ngon-p35476045.html?itm_campaign=HMP_YPD_TKA_PLA_UNK_ALL_UNK_UNK_UNK_UNK_X.54762_Y.456704_Z.1021600_CN.Product-Ads-14%252F06%252F2021&itm_medium=CPC&itm_source=tiki-ads&spid=53087400"); //full
        //driver.get("https://tiki.vn/tai-nghe-bluetooth-chup-tai-sony-wh-1000xm4-hi-res-noise-canceling-hang-chinh-hang-p62445418.html?spid=63857939&__s=ssdp"); //color
        //driver.get("https://tiki.vn/noi-chien-khong-dau-philips-hd9200-90-hang-chinh-hang-p74594174.html?spid=79229268&__s=ssdp"); //none

        driver.get("https://tiki.vn");
        driver.manage().window().maximize();

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement skipBtn = driver.findElement(By.id("onesignal-slidedown-cancel-button"));
        skipBtn.click();

        WebElement buttonLoginAndSignUp = driver.findElement(By.xpath("//span[text()='Đăng Nhập / Đăng Ký']"));
        buttonLoginAndSignUp.click();
        WebElement buttonLogin = driver.findElement(By.xpath("//button[text()='Đăng nhập']"));
        buttonLogin.click();

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement textBoxUserName = driver.findElement(By.xpath("//input[@placeholder='Số điện thoại']"));
        textBoxUserName.sendKeys("0938765013");
        WebElement buttonNext = driver.findElement(By.xpath("//button[text()='Tiếp Tục']"));
        buttonNext.click();

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement texBoxPassword = driver.findElement(By.xpath("//input[@placeholder='Mật khẩu']"));
        texBoxPassword.sendKeys("123456a@A");

        WebElement login = driver.findElement(By.xpath("//button[text()='Đăng Nhập']"));
        login.click();

        try {
            wait.until(ExpectedConditions.invisibilityOf(login));
        } catch (NoSuchElementException e) {
            e.getMessage();
        }
        Thread.sleep(1000);

*//*        WebElement cartBtn = driver.findElement(By.xpath("//span[text()='Giỏ Hàng']"));
        cartBtn.click();
        Thread.sleep(1000);

        List<WebElement> listOnCart = driver.findElements(By.xpath("//div[@class = 'intended__images false']//div[@class = 'intended__content']//ancestor::div[@class='col-1']/following-sibling::div[@class='col-4']"));
        for (int i = 0; i < listOnCart.size(); i++) {
            String priceActualStr = listOnCart.get(i).getText();
            System.out.println(priceActualStr);
            priceActualStr = priceActualStr.replaceAll("đ", "");
            priceActualStr = priceActualStr.replace(".", "");
            double priceActual = Double.parseDouble(priceActualStr);
            System.out.println(priceActual);
        }*//*


        driver.quit();
    }*/
    public interface DateValidator {
        boolean isValid(String dateStr);
    }

    public static class DateValidatorUsingLocalDate implements DateValidator {
        private DateTimeFormatter dateFormatter;

        public DateValidatorUsingLocalDate(DateTimeFormatter dateFormatter) {
            this.dateFormatter = dateFormatter;
        }

/*        @Override
        public boolean isValid(String dateStr) {
            try {
                LocalDate.parse(dateStr, this.dateFormatter);
            } catch (DateTimeParseException e) {
                return false;
            }
            return true;
        }*/
        @Override
        public boolean isValid(String dateStr) {
            try {
                this.dateFormatter.parse(dateStr);
            } catch (DateTimeParseException e) {
                return false;
            }
            return true;
        }
    }

    public static void main(String[]agrs) throws MalformedURLException {
/*        String pathToRootDirectory = System.getProperty("user.dir");
        System.setProperty("webdriver.chrome.driver", pathToRootDirectory + File.separator + "drivers" + File.separator + "win_chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://viettuts.vn/java/array-trong-java");
        System.out.println("This product did not add to the Cart yet." + "\n" + driver.getCurrentUrl());

        String userDate = "28/02/1956";*/
/*        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/uuuu").withResolverStyle(ResolverStyle.STRICT);
        DateValidator validator = new DateValidatorUsingLocalDate(dateFormatter);

        if (validator.isValid(userDate)) {
            LocalDate minDate = LocalDate.of(1956,1,1).minusDays(1);
            LocalDate maxDate = LocalDate.of(2021,12,31).plusDays(1);
            LocalDate strDate = LocalDate.parse(userDate, DateTimeFormatter.ofPattern("dd/MM/uuuu"));
            //System.out.println(strDate.compareTo(minDate));
            if (strDate.compareTo(minDate) > 0 && strDate.compareTo(maxDate) < 0) {
                System.out.println("This is valid date");
            }
            else {
                System.out.println("Please input again");
            }
        }
        else {
            System.out.println("This is valid format date");
        }*/
        //GenericValidator.isDate(userDate, "dd/MM/uuuu",true);
/*        if (GenericValidator.isDate(userDate, "dd/MM/yyyy",true)) {
            LocalDate minDate = LocalDate.of(1956,1,1).minusDays(1);
            LocalDate maxDate = LocalDate.of(2021,12,31).plusDays(1);
            LocalDate strDate = LocalDate.parse(userDate, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
            //System.out.println(strDate.compareTo(minDate));
            if (strDate.compareTo(minDate) > 0 && strDate.compareTo(maxDate) < 0) {
                System.out.println("This is valid date");
            }
            else {
                System.out.println("Please input again");
            }
        }
        else {
            System.out.println("This is invalid format date");
        }*/

        DesiredCapabilities dc = new DesiredCapabilities();

        //dc.setCapability(MobileCapabilityType.DEVICE_NAME, "5cab86a2");
        dc.setCapability(MobileCapabilityType.DEVICE_NAME, "R58M73SWCWY");
        dc.setCapability("platformName", "android");
        dc.setCapability("appPackage", "vn.momo.platform.test");
        dc.setCapability("appActivity", "vn.momo.platform.MainActivity");
        dc.setCapability("newCommandTimeout", 60*2);

        AndroidDriver<AndroidElement> ad = new AndroidDriver<AndroidElement>(new URL("http://127.0.0.1:4723/wd/hub"),dc);


        //ad.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        //MobileElement obKhamPhaBtn = (MobileElement) ad.findElement(By.id("Khám phá ngay/Button"));
        //Assert.assertEquals(obKhamPhaBtn.getText(), "Khám phá ngay");

/*        Wait wait = new FluentWait(ad)
                .withTimeout(10, TimeUnit.SECONDS)
                .pollingEvery(250, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class)
                .ignoring(TimeoutException.class);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.view.ViewGroup[@content-desc=\"Khám phá ngay/Button\"]")));*/

        ad.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        MobileElement obKhamPhaBtn = (MobileElement) ad.findElement(By.xpath("//android.view.ViewGroup[@content-desc=\"Khám phá ngay/Button\"]"));
        obKhamPhaBtn.click();

        ad.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        MobileElement phoneNumberField = (MobileElement) ad.findElement(By.xpath("//android.widget.EditText[@content-desc=\"Số điện thoại/TextInput\"]"));
        phoneNumberField.sendKeys("0938765013");

        ad.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        MobileElement nextBtn = (MobileElement) ad.findElement(By.xpath("//android.view.ViewGroup[@content-desc=\"Tiếp tục/Button\"]"));
        nextBtn.click();

/*        ad.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        MobileElement callMeBtn = (MobileElement) ad.findElement(By.xpath("//android.view.ViewGroup[@content-desc=\"Gọi cho tôi/Button\"]"));
        callMeBtn.click();*/

        ad.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        MobileElement otpField = (MobileElement) ad.findElement(By.xpath("//android.widget.EditText[@content-desc=\"OTPTextInput\"]"));
        //otpField.click();

        TouchAction action = new TouchAction(ad);
        action.press(element(otpField));
        action.waitAction(waitOptions(ofSeconds(10)));
        action.release();
        action.perform();

        otpField.sendKeys("0000");

        ad.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        MobileElement pwdField = (MobileElement) ad.findElement(By.xpath("//android.widget.EditText[@content-desc=\"Nhập mật khẩu/TextInput\"]"));
        pwdField.sendKeys("000000");

        //ad.quit();


//        MobileElement obText = (MobileElement) ad.findElementByXPath("//android.widget.TextView[@content-desc=\"Chuyển tiền nhanh Thanh toán 1 chạm/Text\"]");
//
//        Assert.assertEquals(obText.getText(), "Chuyển tiền nhanh\n" + "Thanh toán 1 chạm");

/*        MobileElement obKhamPhaBtn = (MobileElement) ad.findElementByXPath("//android.view.ViewGroup[@content-desc=\"Khám phá ngay/Button\"]");

        MobileElement obPhoneNumberField = (MobileElement) ad.findElementByXPath("//android.widget.EditText[@content-desc=\"Số điện thoại/TextInput\"]");
        obPhoneNumberField.sendKeys("0938765013");

        MobileElement obNextBtn = (MobileElement) ad.findElementByXPath("//android.view.ViewGroup[@content-desc=\"Tiếp tục/Button\"]");
        obNextBtn.click();*/

    }


}
