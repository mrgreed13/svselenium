import static io.restassured.RestAssured.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import io.restassured.RestAssured;
import io.restassured.config.DecoderConfig;
import lombok.extern.slf4j.Slf4j;
import models.baseData;
import models.getOAItemAtHome;
import models.plainOldJavaObject;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import java.util.ArrayList;

@Slf4j
public class apiTesting {

    private static final String mobileEndpoint = "http://api-dev.mservice.io/routerx-internal-app-api-merchant/api";


    @Test
    public void verifyGetOAAtHomeAPI(){
        baseData data = new baseData(
                10,
                0,
                10.7741709,
                106.7014805
        );
        getOAItemAtHome request = new getOAItemAtHome(
                "tdmm-get-home-recommend-oas",
                "vi",
                "0938765013",
                data
        );

        given().body(request).
                when().
                post(mobileEndpoint).
                then().
                log().
                headers().
                assertThat().
                statusCode(200).
                header("content-type", equalTo("application/json")).
                body("status", equalTo(0)).
                body("message", equalTo("Thành công")).
                body("data.content.size()", greaterThan(0)).
                body("data.content.id", everyItem(notNullValue())).
                body("data.content.name", everyItem(notNullValue())).
                body("data.content.name[0]", equalTo("Lẩu và Nướng"));

        ArrayList response = given().body(request).
                when().
                post(mobileEndpoint).
                then().
                extract().path("data.content.name");
        System.out.println(response.get(0));

    }
/*    @Before
    public void init() {
        RestAssured.baseURI = "http://api-dev.mservice.io/routerx-internal-app-api-merchant";
        RestAssured.basePath = "/api";
        //RestAssured.port = 8080;
    }

    @Test
    public static void getResponseStatusFromPOSTMethod(){
        Response res = given()
                .contentType(ContentType.JSON)
                .when()
                .body(student)
                .post();
    }*/
    @Test
    public void getDeserializedProduct(){
        String endpoint = "http://127.0.0.1/api_testing/product/read_one.php";
        //"id":"2","name":"Cross-Back Training Tank","description":"The most awesome phone of 2013!","price":"299.00","category_id":"2","category_name":"Active Wear - Women"
        plainOldJavaObject expectedProduct = new plainOldJavaObject(
                2,
                "Cross-Back Training Tank",
                "The most awesome phone of 2013!",
                299.00,
                2,
                "Active Wear - Women"
        );
        plainOldJavaObject acutalProduct =
                given().
                    queryParam("id", "2").
                when().
                        get(endpoint).
                            as(plainOldJavaObject.class);

        assertThat(acutalProduct, samePropertyValuesAs(expectedProduct));
    }

    @Test
    public  void getCategories(){
        String endPoint = "http://127.0.0.1/api_testing/category/read.php";
        var response = given().when().get(endPoint).then();
        response.log().body();
    }

    @Test
    public void getProduct(){
        String endPoint = "http://127.0.0.1/api_testing/product/read_one.php";
        //for line 83
        String endpoint = "http://127.0.0.1/api_testing/product/read.php";
        var response =
                given().
                    queryParam("id", 2).
                when().
                    get(endPoint).
                then();
        response.log().body();

        //statusCodeAssertion
        given().
                queryParam("id", 2).
        when().
                get(endPoint).
        then().
        assertThat().
                statusCode(200);

        //import static method Matchers.equalTo - Hamcrest/ Rest-assured lib that can help us to compare
        //basicBodyAssertion
        given().
                queryParam("id", 2).
        when().
                get(endPoint).
        then().
        assertThat().
                statusCode(200).
                body("id", equalTo("2")).
                body("name", equalTo("Cross-Back Training Tank")).
                body("description", equalTo("The most awesome phone of 2013!")).
                body("price", equalTo("299.00")).
                body("category_id", equalTo("2")).
                body("category_name", equalTo("Active Wear - Women"));

        //complexBodyAssertion - nested data in data
        given().
        when().
                get(endpoint).
        then().
                log().
                    body().
                    assertThat().
                        statusCode(200).
                        body("records.size()", equalTo(19));

        //make sure size is greater than 1, always have response
        given().
        when().
                get(endpoint).
        then().
            log().
                body().
                assertThat().
                    statusCode(200).
                    body("records.size()", greaterThan(0)).
                    body("records.id", everyItem(notNullValue())).  //every id always have value
                    body("records.name", everyItem(notNullValue())).
                    body("records.description", everyItem(notNullValue())).
                    body("records.price", everyItem(notNullValue())).
                    body("records.category_id", everyItem(notNullValue())).
                    body("records.category_name", everyItem(notNullValue())).
                    body("records.id[0]", equalTo("20"));

        //verify response header
        given().
        when().
                get(endpoint).
        then().
                log().
                    headers().
                    assertThat().
                        statusCode(200).
                        header("Content-Type", equalTo("application/json; charset=UTF-8")).
                        body("records.size()", greaterThan(0)).
                        body("records.id", everyItem(notNullValue())).  //every id always have value
                        body("records.name", everyItem(notNullValue())).
                        body("records.description", everyItem(notNullValue())).
                        body("records.price", everyItem(notNullValue())).
                        body("records.category_id", everyItem(notNullValue())).
                        body("records.category_name", everyItem(notNullValue())).
                        body("records.id[0]", equalTo("20"));
    }

    @Test
    public void createProduct(){
        String endPoint = "http://127.0.0.1/api_testing/product/create.php";
        //need Java 15+
        String body = """
                {
                "name": "Water Bottle",
                "description": "Blue water bottle. Holds 64 ounces",
                "price": 12,
                "category_id": 3
                }
                """;
        var response =
                given().
                    body(body).
                when().post(endPoint).
                then();
        response.log().body();
    }

    @Test
    public void updateProduct(){
        String endPoint = "http://127.0.0.1/api_testing/product/update.php";
        String body = """
                {
                "id": 19,
                "name": "Water Bottle",
                "description": "Blue water bottle. Hold 64 ounces",
                "price": 15,
                "category_id": 3
                }
                """;

        var response = given().body(body).when().put(endPoint).then();
        response.log().body();
    }

    @Test
    public void deleteProduct(){
        String endPoint = "http://127.0.0.1/api_testing/product/delete.php";
        String body = """
                {
                "id": 19
                }
                """;
        var response = given().body(body).when().delete(endPoint).then();
        response.log().body();
    }

   /* @Test
    public void createSerializedProduct(){
        String endPoint = "http://127.0.0.1/api_testing/product/create.php";
        plainOldJavaObject pojo = new plainOldJavaObject(
                "Water Bottle",
                "Blue water bottle. Hold 64 ounces",
                12,
                3

        );

        var response = given().body(pojo).when().post(endPoint).then();
        response.log().body();
    }*/

    @Test
    public void getTheFisrtOAName(){
        baseData data = new baseData(
                10,
                0,
                10.7725168,
                106.6980208
        );
        getOAItemAtHome request = new getOAItemAtHome(
                "tdmm-get-home-recommend-oas",
                "vi",
                "0938765013",
                data
        );

        var resposne = given().body(request).
                when().post(mobileEndpoint).
                then();
        resposne.log().body();

        ArrayList<String> oaName = given().body(request).
                when().post(mobileEndpoint).
                then().extract().path("data.content.name");
        System.out.println("The first OA name: " + oaName.get(0));
    }
    @Test
    public void getOAName(){
        JSONObject request = new JSONObject();
        JSONObject data = new JSONObject();

        request.put("requestType", "tdmm-get-home-recommend-oas");
        request.put("language", "vi");
        request.put("userIdApp", "0938765013");

        data.put("pageSize", "10");
        data.put("pageNumber", "0");
        data.put("lat", "10.7725168");
        data.put("lon", "106.6980208");

        request.put("data", data);

        ArrayList<String> oaName = given().body(request.toJSONString()).when().post(mobileEndpoint).then().extract().path("data.content.name");
        System.out.println(oaName.get(0));
    }

    /*@Test
    public static void getStatusCodePostMethod(){
        JSONObject request = new JSONObject();
        JSONObject data = new JSONObject();

        try {
            request.put("requestType", "tdmm-get-home-recommend-oas");
            request.put("language", "vi");
            request.put("userIdApp", "0938765013");

            data.put("pageSize", "10");
            data.put("pageNumber", "0");
            data.put("lat", "10.7725168");
            data.put("lon", "106.6980208");

            request.put("data", data);

            given().
*//*                header("Content-Type", "application/json").
                contentType(ContentType.JSON).
                accept(ContentType.JSON).*//*
        body(request.toJSONString()).
                    when().
                    post(devUrl).
                    then().statusCode(200);
        } catch (Exception e) {
            System.out.println(e);
        }
*//*        int statusCode = given().
                when().get(url).getStatusCode();
        given().when().get(url).then().assertThat().statusCode(200)*//*
    }


    @Test
    public static void getResponseBodyPostMethod() {
        JSONObject request = new JSONObject();
        JSONObject data = new JSONObject();

        try {
            request.put("requestType", "tdmm-get-home-recommend-oas");
            request.put("language", "vi");
            request.put("userIdApp", "0938765013");

            data.put("pageSize", "10");
            data.put("pageNumber", "0");
            data.put("lat", "10.7725168");
            data.put("lon", "106.6980208");

            request.put("data", data);

            given().
*//*                header("Content-Type", "application/json").
                contentType(ContentType.JSON).
                accept(ContentType.JSON).*//*
        body(request.toJSONString()).
                    when().
                    post(devUrl).
                    then().statusCode(200).log().body();
        } catch (Exception e) {
            System.out.println(e);
        }
    }



    @Test
    public static void getResponseHeaders(){
        JSONObject request = new JSONObject();
        JSONObject data = new JSONObject();

        try {
            request.put("requestType", "tdmm-get-home-recommend-oas");
            request.put("language", "vi");
            request.put("userIdApp", "0938765013");

            data.put("pageSize", "10");
            data.put("pageNumber", "0");
            data.put("lat", "10.7725168");
            data.put("lon", "106.6980208");

            request.put("data", data);

            System.out.println(given().
*//*                header("Content-Type", "application/json").
                contentType(ContentType.JSON).
                accept(ContentType.JSON).*//*
        body(request.toJSONString()).
                    when().
                    post(devUrl).
                    then().extract().headers());
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Test
    public static void getResponseContentType(){
        JSONObject request = new JSONObject();
        JSONObject data = new JSONObject();

        try {
            request.put("requestType", "tdmm-get-home-recommend-oas");
            request.put("language", "vi");
            request.put("userIdApp", "0938765013");

            data.put("pageSize", "10");
            data.put("pageNumber", "0");
            data.put("lat", "10.7725168");
            data.put("lon", "106.6980208");

            request.put("data", data);

            System.out.println(given().
*//*                header("Content-Type", "application/json").
                contentType(ContentType.JSON).
                accept(ContentType.JSON).*//*
        body(request.toJSONString()).
                    when().
                    post(devUrl).
                    then().extract().contentType());
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Test
    public static void getSpecificPartOfResponseBody(){

*//*        ArrayList<String> amounts = when().get(devUrl).then().extract().path("result.statements.AMOUNT") ;
        int sumOfAll=0;
        for(String a:amounts){

            System.out.println("The amount value fetched is "+a);
            sumOfAll=sumOfAll+Integer.valueOf(a);
        }
        System.out.println("The total amount is "+sumOfAll);*//*
        JSONObject request = new JSONObject();
        JSONObject data = new JSONObject();

        request.put("requestType", "tdmm-get-home-recommend-oas");
        request.put("language", "vi");
        request.put("userIdApp", "0938765013");

        data.put("pageSize", "10");
        data.put("pageNumber", "0");
        data.put("lat", "10.7725168");
        data.put("lon", "106.6980208");

        request.put("data", data);

        ArrayList<String> oaName = given().body(request.toJSONString()).when().post(devUrl).then().extract().path("data.content.name");
        System.out.println(oaName.get(0));

    }

    @Test
    public static void getResponseTime(){
        System.out.println("The time taken to fetch the response "+get(devUrl)
                .timeIn(TimeUnit.MILLISECONDS) + " milliseconds");
    }

    @Test
    public void apiTest1() {
        Response response = get("https://reqres.in/api/users?page=2");
        System.out.println("Response: " + response.asString());
        System.out.println("Status code: " + response.getStatusCode());
        System.out.println("Body: " + response.getBody().asString());
        System.out.println("Time taken: " + response.getTime());
        System.out.println("Header: " + response.getHeader("content-type"));

        int statusCode = response.getStatusCode();
        Assert.assertEquals("200", statusCode);
    }

    @Test
    public void testAPI2() {
        given().
                get("https://reqres.in/api/users?page=2").
                then().statusCode(200);
    }*/
}
