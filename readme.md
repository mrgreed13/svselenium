config: -Denvironment=dev -Dbrowser=chrome

setup Git
1. Kiểm tra đã có key hay chưa? C:\Users\owner\.ssh
Đã có. Bật copy
Chưa có qua step 2
2. google: git generate ssh key
   https://inchoo.net/dev-talk/how-to-generate-ssh-keys-for-git-authorization/
Ctrl C = Shift Insert
3. Quay lại folder: C:\Users\owner\.ssh
4. Bật file id_rsa.publisher
5. Copy
6. Quay về gitLab
7. Vào Account/Preference/SSH Keys
8. Paste key vào Key field
9. Quay lại project
10. Chọn Clone
11. Copy SSH
12. Paste vào git